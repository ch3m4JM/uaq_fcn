<?php

//UTF-8
header("Content-type: text/html; charset=utf8");
//conexión
    include("../../php/conect.php");


//Si se manda a llamar una funcion
if(isset($_GET['funcion'])){
    $funcion=$_GET['funcion'];
    switch($funcion){
        case 'Subir':
            subirArchivo();
            break;
            
        case 'consultarArchivo':
            consultarArchivo();
            break;
            
        case 'validarArchivos':
            validarArchivos();
            break;
        
        case 'cambiarEstado':
            cambiarEstado();
            break;
        
        case 'registrarPago':
            registrarPago();
            break;
            
        case 'consultarDatosPago':
            consultarDatosPago();
            break;
            
        case 'consultarPrograma':
            consultarPrograma();
            break;
            
        default:
            echo "La funcion llamada no existe: ".$funcion;
            break;
    }
    
}

function subirArchivo(){
        //Verífica que se halla recivido un archivo
    if(isset($_FILES)){
       
        //Se obtiene la matricula del aspirante
        session_start();
        $Matricula=$_SESSION['pk'];
        
        //Se asigna el archivo a una variable
        $idInput=$_POST['id'];
        $result=$_FILES[$idInput];

        //Directorio donde se guardara el archivo
        $dir_subida = '../PDF/';

        //Nombre que tendra el archivo
        //$nombre=basename($result['name']);
        $nombre=$Matricula."_".$_POST['id'];

        //Directorio final de subida de archivo
        $fichero_subido = $dir_subida . $nombre;

        //Verifica si se movio el archivo al servidor
        if (move_uploaded_file($result['tmp_name'], $fichero_subido)) {
            //se registra el movimieno en la base de datos
            $SQL="update documentos set ".$_POST['id']."=1 where fk_matricula='".$Matricula."';";
            if($s = executeQuery($SQL)){
                limpiarStm($s);
                echo json_encode(array("Archivo subido",$_POST['id']));
            }
        } else {
            echo "¡Posible ataque de subida de ficheros!\n";
            echo 'Más información de depuración:';
            print_r($result);
        }

        //echo "<br>".$_POST['id'];
    }else echo "No se recivio el archivo";
}

function consultarArchivo(){
    if(isset($_GET['doc'])){
        echo $_GET['doc'];
    }else echo "no se recivio la variable";
}

function validarArchivos(){
    //Session
    session_start();

    if(isset($_SESSION['pk'])){
            //Se obtiene la matricula
            $Matricula=$_SESSION['pk'];

            //Sentencia SQL para buscar campos
            $Sentencia="select * from documentos where fk_matricula='".$Matricula."';";

            //Ejecutar busqueda
            echo resultQueryJson($Sentencia);
        }else echo "salir";
}

function cambiarEstado(){
    //Session
    session_start();

    if(isset($_SESSION['pk'])){
        //Se obtiene la matricula
        $Matricula=$_SESSION['pk'];
        
        //Se obtiene el estado a cambiar
        $Estado=$_GET['estado'];

        $SQL="update apartados set documentos=".$Estado." where fk_matricula='".$Matricula."';";
        
        if($s1 = executeQuery($SQL)){
            limpiarStm($s1);
            echo "Hecho";
        }
    }else echo "salir";
}

function registrarPago(){
    //Session
    session_start();

    if(isset($_SESSION['pk'])){
        //Se obtiene la matricula
        $Matricula=$_SESSION['pk'];
        //Se obtienen los datos del formulario
        $Banco=$_POST['inputBanco'];
        $Fecha=$_POST['inputFecha'];
        $Folio=$_POST['inputFolio'];
        $Referencia=$_POST['inputReferencia'];
        $Expediente=$_POST['inputExpediente'];
        //Sentencia SQL
        $SQL="update alumnos set pago_banco='".$Banco."',pago_fecha='".$Fecha."',pago_folio='".$Folio."',pago_referencia1='".$Referencia."',pago_expediente='".$Expediente."' where pk_matricula='".$Matricula."';";
        
        if($s2 = executeQuery($SQL)){
            limpiarStm($s2);
            echo "Hecho";
        }
    }else echo "salir";  
}

function consultarDatosPago(){
    //Session
    session_start();

    if(isset($_SESSION['pk'])){
        //Se obtiene la matricula
        $Matricula=$_SESSION['pk'];

        //Sentencia SQL
        $SQL="select pago_banco,pago_fecha,pago_folio,pago_referencia1,pago_expediente from alumnos where pk_matricula='".$Matricula."';";
        
        if($resultado = resultQueryJson($SQL)){
            echo $resultado;
        }
    }else echo "salir";  
}

function consultarPrograma(){
    //Session
    session_start();
    
    if(isset($_SESSION['pk'])){
                //matricula
                $Matricula=$_SESSION['pk'];
                
                //consulta de datos
                $sqlDatos="SELECT alumnos.nombre,programas.pk_programa,programas.nombre,periodos.fecha_inicio,periodos.fecha_fin,pais,alumnos.trabaja FROM `alumnos` INNER JOIN programas ON (alumnos.fk_programa=programas.pk_programa) INNER JOIN periodos ON (programas.pk_programa = periodos.fk_programa) WHERE pk_matricula='".$Matricula."';";
                
//                echo $sqlDatos;
                echo resultQueryJson($sqlDatos);
        }else echo "salir";
    }
?>
