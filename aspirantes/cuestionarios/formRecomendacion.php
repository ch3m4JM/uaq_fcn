<?php
    if(isset($_GET['c']) && isset($_GET['m']) && isset($_GET['n'])){
        //conexion
        include("../php/conect.php");

        //Obtener Datos
        $Clave=$_GET['c'];
        $Matricula=$_GET['m'];
        $Referencia=$_GET['n'];
        
        //Verificar Disponibilidad
        $SQLverificar="select * from recomendaciones where fk_matricula='".$Matricula."' and estado='En espera' and clave='".$Clave."' and fk_referencia='".$Referencia."';";
        
        $Opcion="aaa";
        $Valor = getCount($SQLverificar);
        if($Valor > 0){
            $Opcion="Disponible";
            //Consultar Datos
            $SQL="select alumnos.nombre,alumnos.apellido_p,alumnos.apellido_m,correo,programas.nombre from alumnos inner join programas on (alumnos.fk_programa = programas.pk_programa) where alumnos.pk_matricula='".$Matricula."';";

            $Datos = resultQuery($SQL);

            //    echo "Clave: ".$Clave." Matricula: ".$Matricula." Referencia: ".$Referencia;
            $NombreAspirante = $Datos[0][0]." ".$Datos[0]['apellido_p']." ".$Datos[0]['apellido_m'];
            $CorreoAspirante = $Datos[0]['correo'];
            $NombrePrograma = $Datos[0][4];
            
            //DeepMint Change, bug fixed 
            session_start();
            $_SESSION['pk'] = "Temporary";
            
        }else{
            $Opcion="No disponible";
//            $Opcion=$SLQverificar." ;".$Valor;
        }
        
    }else{
                header("location: ../index.php");
        }
?>

    <!DOCTYPE html>
    <html lang="esp">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>UAQ REGISTRO ASPIRANTES</title>

        <!-- Bootstrap Core CSS -->
        <!--    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
        <link href="css/bootstrapV4.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">


        <!-- Custom CSS -->
        <link href="css/stylish-portfolio.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/file-input/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/file-input/explorer/theme.css" media="all" rel="stylesheet" type="text/css" />

        <!-- Custom Fonts -->
        <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>


    <body>
        <div id="cargando"></div>

        <!-- Navigation -->
        <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle">
        <i class="fa fa-bars"></i>
    </a>
        <nav id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle">
                    <i class="fa fa-times"></i>
                </a>
                <li class="sidebar-brand">
                </li>
                <li>
                    <a class="js-scroll-trigger" href="../">Proceso de admisión</a>
                </li>
                <li>
                    <a class="js-scroll-trigger" href="#contact" onclick=$( #menu-close).click();>Contacto</a>
                </li>
                <li>
                    <a class="js-scroll-trigger" href="preguntasFrecuentes.html" onclick=$( #menu-close).click();>Preguntas frecuentes</a>
                </li>
            </ul>
        </nav>

        <!-- About -->
        <header class="h-100 d-inline-block mw-100" style="width: 100%">
            <div class="imgHeader">
                <h1 class="title-header">Recomendación</h1>
            </div>

            <div class="container text-center">
                <h2>Información</h2>
                <p class="lead">
                    <strong class="blueWords">Usted fue elegido por <?php echo $NombreAspirante; ?> para contestar este formulario.</strong><br> Carta de recomendación para solicitar el ingreso al posgrado de
                    <strong><?php echo $NombrePrograma; ?></strong> en la Facultad de Ciencias Naturales, FCN. UAQ.</p>

                <div class="container text-center">
                    <h2>Indicaciones</h2>
                    <p class="lead"><strong class="blueWords">Por favor conteste con sinceridad todo lo que se solicita.</strong></p>
                </div>
                <!--            Explicacion Grafica-->

                <div class="text-center">
                    <br>
                    <a href="#campos" class="btn btn-primary btn-dark btn-lg js-scroll-trigger">Comenzar</a>
                </div>
            </div>
            <!-- /.container -->
        </header>

        <section id="campos" class="container">
            <div class="col-md-12">

                <form id="formularioRecomendacion">

                    <h2 class="text-info"><strong>Información del aspirante:</strong></h2>
                    <div class="form-group">
                        <label for="inputDescripcion">Describa como y desde cuando conoce a <?php echo $NombreAspirante;?>:</label>
                        <textarea type="text" id="inputDescripcion" name="inputDescripcion" class="form-control" required></textarea>
                    </div>

                    <div class="form-group">
                        <label for="tablaEscalas">Evalue a <?php echo $NombreAspirante;?> de acuerdo a la siguiente escala:</label>

                        <table id="tabla escalas" class="table table-responsive table table-hover table-striped">
                            <thead class="bg-info" style="color: white;">
                                <tr>
                                    <th class="text-center">Aspecto a evaluar</th>
                                    <th class="text-center">Excelente</th>
                                    <th class="text-center">Bueno</th>
                                    <th class="text-center">Regular</th>
                                    <th class="text-center">Deficiente</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center">
                                    <td class="text-left">Conocimientos</td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioConocimientos" id="radioConocimientos1" value="Excelente" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioConocimientos" id="radioConocimientos2" value="Bueno" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioConocimientos" id="radioConocimientos3" value="Regular" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioConocimientos" id="radioConocimientos4" value="Deficiente" required>
                                    </td>
                                </tr>

                                <tr class="text-center">
                                    <td class="text-left">Dedicación al trabajo</td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioDedicacion" id="radioDedicacion1" value="Excelente" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioDedicacion" id="radioDedicacion2" value="Bueno" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioDedicacion" id="radioDedicacion3" value="Regular" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioDedicacion" id="radioDedicacion4" value="Deficiente" required>
                                    </td>
                                </tr>

                                <tr class="text-center">
                                    <td class="text-left">Habilidades para comunicarse</td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioComunicacion" id="radioComunicacion1" value="Excelente" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioComunicacion" id="radioComunicacion2" value="Bueno" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioComunicacion" id="radioComunicacion3" value="Regular" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioComunicacion" id="radioComunicacion4" value="Deficiente" required>
                                    </td>
                                </tr>

                                <tr class="text-center">
                                    <td class="text-left">Iniciativa</td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioIniciativa" id="radioIniciativa1" value="Excelente" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioIniciativa" id="radioIniciativa2" value="Bueno" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioIniciativa" id="radioIniciativa3" value="Regular" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioIniciativa" id="radioIniciativa4" value="Deficiente" required>
                                    </td>
                                </tr>

                                <tr class="text-center">
                                    <td class="text-left">Perseverancia</td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioPerseverancia" id="radioPerseverancia1" value="Excelente" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioPerseverancia" id="radioPerseverancia2" value="Bueno" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioPerseverancia" id="radioPerseverancia3" value="Regular" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioPerseverancia" id="radioPerseverancia4" value="Deficiente" required>
                                    </td>
                                </tr>

                                <tr class="text-center">
                                    <td class="text-left">Actitud</td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioActitud" id="radioActitud1" value="Excelente" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioActitud" id="radioActitud2" value="Bueno" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioActitud" id="radioActitud3" value="Regular" required>
                                    </td>
                                    <td>
                                        <input class="form-check-input text-center" type="radio" name="radioActitud" id="radioActitud4" value="Deficiente" required>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group">
                        <label for="inputHabilidadesDebilidades">Comente las principales habilidades y debilidades de <?php echo $NombreAspirante;?> que usted considere importantes para la evaluación:</label>
                        <textarea type="text" id="inputHabilidadesDebilidades" name="inputHabilidadesDebilidades" class="form-control" required></textarea>
                    </div>

                    <h2 class="text-info"><strong>Información personal:</strong></h2>

                    <div class="form-group">
                        <label for="inputNombreRecomendador">Nombre completo *</label>
                        <input type="text" id="inputNombreRecomendador" name="inputNombreRecomendador" class="form-control" placeholder="Escriba su nombre y apellidos" required>
                    </div>

                    <div class="form-group">
                        <label for="inputLugarTrabajo">Lugar de trabajo *</label>
                        <input type="text" id="inputLugarTrabajo" name="inputLugarTrabajo" class="form-control" placeholder="Lugar de trabajo" required>
                    </div>

                    <div class="form-group">
                        <label for="inputTelClavePais" class="">Número de teléfono o celular de contacto *</label>
                        <div class="form-group row">
                            <div class="col-sm-2 ">
                                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                    <div class="input-group-addon"><strong>+</strong></div>
                                    <input type="number" class="form-control required" minlength="2" min="000" max="999" id="inputTelClavePais" name="inputTelClavePais" placeholder="Lada del Pais (2 a 3 dígitos)" pattern="[0-9]{9}" maxlength="2" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
                                </div>
                                <small id="ClaveHelp" class="form-text text-muted">Lada del paíss.</small>
                            </div>
                            <div class="col-sm-3">
                                <input type="number" class="form-control required" id="inputTelLada" name="inputTelLada" placeholder="Lada de región (2 a 3 dígitos)" minlength="2" maxlength="3" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
                                <small id="LadaHelp" class="form-text text-muted">Lada de región.</small>
                            </div>
                            <div class="col-sm-4">
                                <input type="number" class="form-control required" id="inputTelNumero" name="inputTelNumero" placeholder="Número (7 a 10 dígitos)" maxlength="10" minlength="7" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                                <small id="LadaHelp" class="form-text text-muted">Número (7 a 10 dígitos).</small>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" id="hMatricula" name="Matricula" value="<?php echo $Matricula; ?>">
                    <input type="hidden" id="hClave" name="Clave" value="<?php echo $Clave; ?>">
                    <input type="hidden" id="hReferencia" name="Referencia" value="<?php echo $Referencia; ?>">

                    <br>
                    <div class="politica">
                        Aviso de privacidad<br> En cumplimiento a lo establecido en los artículos 6 fracciones II y III y 16 segundo párrafo de la Constitución Política de los Estados Unidos Mexicanos, Articulo 14 de la ley de protección de datos personales en posesión de los particulares, la Facultad de Ciencias Naturales de la Universidad Autónoma de Querétaro, con domicilio en Av. de las Ciencias s/n, Col. Juriquilla, C.P. 76230, Querétaro, Querétaro, le informa que los datos personales que recaba con motivo del ejercicio de sus funciones. Por tanto, respecto de dichos datos esta institución, le hace de su conocimiento que: El presente aviso de privacidad tiene como finalidad la protección de los datos personales de los integrantes de la comunidad universitaria, mediante su tratamiento legítimo, controlado e informado, a efecto de garantizar su privacidad, así como tu derecho a la autodeterminación informativa. Derechos ARCO (acceso, rectificación cancelación u oposición) revocación y limitación de uso de los datos personales. El titular podrá ejercer su derecho al acceso, rectificación cancelación u oposición. Así mismo la universidad atenderá las solicitudes que el titular tenga respecto a la revocación de su consentimiento para dar tratamiento, uso o divulgar sus datos personales. Para ejercer los derechos derivados de la protección de datos en posesión de esta institución, podrá dirigirse a la Secretaría Académica de la FCN, la Jefatura de Investigación y Posgrado de la FCN o a la unidad de enlace y acceso a la información, ésta última ubicada en Cerro de las Campanas sin número Colonia Las Campanas edificio Rectoría o llamar por teléfono a 1921200 extensiones 5303, 5305 5371 de la FCN o 3408 de la unidad de enlace, o mediante correo electrónico a secretaria.academica.fcn@uaq.edu.mx, fcn.posgrados@uaq.mx o enlace@uaq.mx en donde se le dará la atención personalizada respecto a los mecanismos para el ejercicio de estos preceptos. “Educo en la Verdad y en el Honor”<br>
                        <div class="col-md-6">
                        <label for="politica">Acepto las politicas de privacidad</label>
                        <input type="checkbox" id="politica" name="politica" required>
                    </div>
                    </div>
                    <br>
                        <div class="form-group text-center">
                            <button type="button" id="btnEnviar" class="btn btn-primary">Enviar recomendación</button>
                        </div>
                </form>
                <input type="hidden" id="opcion" value="<?php echo $Opcion; ?>">
            </div>
        </section>


        <!-- Footer -->
        <footer>
            <div id="contact" class="container">
                <div class="row">
                    <div class="col-lg-10 mx-auto text-center">
                        <h4>
                            <strong>Registro de Aspirantes - Facultad de Ciencias Naturales - UAQ</strong>
                        </h4>
                        <p>Campus Juriquilla
                            <br>Avenida de las Ciencias S/N Juriquilla
                            <br>Delegación Santa Rosa Jáuregui
                            <br>Querétaro, México.
                            <br>C.P. 76230
                        </p>

                        <ul class="list-unstyled">
                            <li>
                                <i class="fa fa-phone fa-fw"></i> (442) 234 29 58
                            </li>
                            <li>
                                <i class="fa fa-phone fa-fw"></i> 192-12-00 exts. 5301 y 5303
                            </li>

                            <!--
                        <li>
                            <i class="fa fa-envelope-o fa-fw"></i>
                            <a href="mailto:name@example.com">name@example.com</a>
                        </li>
-->
                        </ul>
                        <br>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a target="_blank" href="https://www.facebook.com/fcnuaq">
                                <i class="fa fa-facebook fa-fw fa-3x"></i>
                            </a>
                            </li>
                            <li class="list-inline-item">
                                <a target="_blank" href="https://twitter.com/fcnuaq">
                                <i class="fa fa-twitter fa-fw fa-3x"></i>
                            </a>
                            </li>
                            <li class="list-inline-item">
                                <a target="_blank" href="https://www.youtube.com/channel/UC9MYg-zg-R1MLddZhxUL4qQ">
                                <i class="fa fa-youtube fa-fw fa-3x"></i>
                            </a>
                            </li>
                        </ul>
                        <hr class="small">
                        <p class="text-muted">Copyright &copy; Registro de Aspirantes - Facultad de Ciencias Naturales</p>
                    </div>
                </div>


            </div>
            <a id="to-top" href="#top" class="btn btn-dark btn-lg js-scroll-trigger">
            <i class="fa fa-chevron-up fa-fw fa-1x"></i>
        </a>
        </footer>



        <!-- Include jQuery -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <!-- Bootstrap core JavaScript -->

        <script src="vendor/popper/popper.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>


        <script src="js/jquery.validate.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.bootstrap.wizard.js"></script>
        <script src="js/Custom/JS_formReferencias.js"></script>

        <!--    Mensajes-->
        <script src="../js/sweetalert.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for file input -->
        <script src="js/stylish-portfolio.js"></script>


    </body>

    </html>
