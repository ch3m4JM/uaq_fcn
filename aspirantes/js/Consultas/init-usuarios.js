/*---------------------------------------
       DEEP MINT WAS HERE  <(°_°)> 
---------------------------------------*/
//VARIABLES GLOBALES
var itemSaved = [];
var refresh = false;
var pkDeleteUser = "null";
var userLevel = "null";
var userData = [];
/*---------------------------------------
            VALIDAR SESIÓN 
---------------------------------------*/
function Kakunin() { /* Kakunin == Validar en japonés --- función que valida la sesión y hace que el resto de la pagina continue*/
    var url = "php/Consultas/sesion-validate.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        success: function (data) { /* Data contendra el valor true encaso de que se valide la sesión o nanay en caso contrario a travéz del archivo php validar-sesion */
            if (data.usuario_success) {//Sí hay sesión ejecuta las demás funciones
                userLevel = data.level;
                if (data.level == 'Coordinador') {
                    location.href = "consultas.html";
                } else {
                    userData = data.user_data;
                    $("#nombre").val(data.user_data[0][0]);
                    $("#apellido-p").val(data.user_data[0][1]);
                    $("#apellido-m").val(data.user_data[0][2]);
                    $("#mail").val(data.user_data[0][3]);
                    $("#pass").val(data.user_data[0][4]);
                    $(".save-user").attr("name",data.user_data[0][3]);
                    fatarat(data.level);
                    inizializzare();
                }
            } else { //Si no hay sesión nos vamos al login
                //De regreso al login
                location.href = "login.php";
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error nos vamos al login
            //De regreso al login
            location.href = "login.php";
        }

    });
}
Kakunin(); //Ejecutamos la función Kakunin

/*---------------------------------------
            CERRAR SESIÓN 
---------------------------------------*/
function sesionDie() { /*  --- función que aniquila la sesión activa*/
    kuonyesha(2);
    var url = "php/Consultas/sesion-die.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        success: function (data) {
            if (data.success) {
                //De regreso al login
                location.href = "login.php";
            } else {
                Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                kuonyesha(3);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error nos vamos al login
            //De regreso al login
            Materialize.toast("¡Error!, vuelva a intentarlo", 3000);
            kuonyesha(3);
        }

    });
}


/*------------------------------------
   FUNCIÓN PARA EDITAR USUARIO
-------------------------------------*/
function editUserStatic() {
    $(document.getElementById('edit')).addClass('hide');
    $(document.getElementById('cancel')).removeClass('hide');
    $(document.getElementById('save')).removeClass("scale-out");
    $(document.getElementById('save')).addClass("scale-in");

    $(document.getElementById('nombre')).removeAttr("disabled");
    $(document.getElementById('apellido-p')).removeAttr("disabled");
    $(document.getElementById('apellido-m')).removeAttr("disabled");
    $(document.getElementById('mail')).removeAttr("disabled");
    $(document.getElementById('pass')).removeAttr("disabled");
}
/*-----------------------------------------------
   FUNCIÓN PARA CANCELAR ACCIÓN EDITAR USUARIO
-----------------------------------------------*/
function cancelUserStatic() {
    $(document.getElementById('cancel')).addClass('hide');
    $(document.getElementById('edit')).removeClass('hide');
    $(document.getElementById('save')).removeClass("scale-in");
    $(document.getElementById('save')).addClass("scale-out");
    $(document.getElementById('save')).addClass("scale-out");
    var index = parseInt($(document.getElementById('cancel')).attr("value"));
    $(document.getElementById('nombre')).val(userData[0][0]);
    $(document.getElementById('apellido-p')).val(userData[0][1]);
    $(document.getElementById('apellido-m')).val(userData[0][2]);
    $(document.getElementById('mail')).val(userData[0][3]);
    $(document.getElementById('pass')).val(userData[0][4]);
    $(document.getElementById('nombre')).attr("disabled", "disabled");
    $(document.getElementById('apellido-p')).attr("disabled", "disabled");
    $(document.getElementById('apellido-m')).attr("disabled", "disabled");
    $(document.getElementById('mail')).attr("disabled", "disabled");
    $(document.getElementById('pass')).attr("disabled", "disabled");
    Materialize.updateTextFields();

}
/*-------------------------------------------
   FUNCIÓN PARA GUARDAR CAMBIOS DE USUARIO
--------------------------------------------*/
function saveUserStatic(pkUsuario) {

    var check = [];
    check = checkValidateStatic();
    if (check[0] == 1) {
        Materialize.toast(check[1], 3000);
    } else {
        kuonyesha(2);
        var userinfo = [];
        userinfo[0] = $(document.getElementById('nombre')).val();
        userinfo[1] = $(document.getElementById('apellido-p')).val();
        userinfo[2] = $(document.getElementById('apellido-m')).val();
        userinfo[3] = $(document.getElementById('mail')).val();
        userinfo[4] = $(document.getElementById('pass')).val();
        var url = "php/Consultas/usuarios.php"; // El script a dónde se realizará la petición.
        var parametro = {
            "accion": "update_usuario",
            "pk_usuario": pkUsuario,
            "user-data": userinfo
        };
        $.ajax({
            type: "POST",
            url: url,
            data: parametro,
            dataType: "JSON",
            success: function (data) {
                if (data.success) {
                    Materialize.toast("¡Exito!, los cambios han sido efectuados", 3000,'',function(){ location.reload();});
                   
                } else {
                    Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                    pkDeleteUser = "null";
                    kuonyesha(3);
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                Materialize.toast("¡Error!, vuelva a intentarlo", 3000);
                pkDeleteUser = "null";
                kuonyesha(3);
            }

        });
    }
}

/*--------------------------------------
   VALIDAR CAMPOS DE USUARIO
--------------------------------------*/
function checkValidateStatic() {
    var regex = "/[w-.]{2,}@([w-]{2,}.)*([w-]{2,}.)[w-]{2,4}/";
    var meta = [];
    if ($(document.getElementById('nombre')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo nombre debe ser llenado";
    } else if ($(document.getElementById('apellido-p')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo apellido paterno debe ser llenado";
    } else if ($(document.getElementById('apellido-m')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo apellido materno debe ser llenado";
    } else if ($(document.getElementById('mail')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo correo electrónico debe ser llenado";
    } else if ($(document.getElementById('mail')).val().indexOf('@', 0) == -1 || $(document.getElementById('mail')).val().indexOf('.', 0) == -1) {
        meta[0] = 1;
        meta[1] = "!Alerta¡, introduzca un correo válido";

    } else if ($(document.getElementById('pass')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo contraseña debe ser llenado";
    } else {
        meta[0] = 0;
        meta[1] = "All right!";

    }
    return meta;
}
/*---------------------------------------
    CARGAR USUARIOS y NOMBRE
---------------------------------------*/
function fatarat(level) { /* fatarat == periodos en Árabe, Función que consulta todos los periodos*/
    var url = "php/Consultas/usuarios.php"; // El script a dónde se realizará la petición.
    var parametro;
    if (level == 'Admin') {
        parametro = {
            "accion": "consulta-everything"
        };
    } else if (level == 'Jefe') {
        parametro = {
            "accion": "consulta-programas"
        };
    }
    $.ajax({
        type: "POST",
        url: url,
        data: parametro,
        dataType: "JSON",
        success: function (data) {
            var success;
            if (level == 'Admin') {
                success = data.usuarios_success;
            } else if (level == 'Jefe') {
                success = data.programas_success;
            }
            if (success) {
                if (level == 'Admin') {
                    $('#banner-admin').removeClass("hide");
                    $('.div-nuevo-usuario').fadeOut("fast");
                    $('.div-consultar-usuarios').fadeOut("fast");
                    if (!refresh) {
                        $('#usuario_name').append(data.nombre);
                        $('#usuario_name_mobile').append(data.nombre);
                        $.each(data.programas, function (index) {
                            var $option = $("<option>", {
                                value: data.programas[index][0],
                                text: data.programas[index][1]
                            });
                            $('#program').append($option);
                        });
                        inizializzare();
                    } else {
                        itemSaved = null;
                    }
                    itemSaved = data.usuarios;
                    $('#colap-coordi').empty();
                    $('#colap-admin').empty();
                    $('#colap-jefe').empty();

                    $.each(data.usuarios, function (index) {
                        var $li = $("<li>", {
                                class: "hoverable"
                            }),
                            $divColHeader = $("<div>", {
                                class: "collapsible-header"
                            }),
                            $iColHeader = $("<i>", {
                                class: "material-icons"
                            }),
                            $spanColHeader = $("<span>", {
                                class: "new badge mybadge hide-on-small-only"
                            }),
                            $divColBody = $("<div>", {
                                class: "collapsible-body"
                            }),
                            $divMobile = $("<div>", {
                                class: "card white-text blue center hide show-on-small"
                            }),
                            $h6Mobile = $("<h6>", {
                                class: "badge-style-h"
                            }),
                            $divRow = $("<div>", {
                                class: "row"
                            }),
                            $divNombre = $("<div>", {
                                class: "input-field col s12"
                            }),
                            $iNombre = $("<i>", {
                                class: "material-icons prefix",
                                text: "face"
                            }),
                            $inputNombre = $("<input>", {
                                value: data.usuarios[index][0],
                                type: "text",
                                id: "nombre" + data.usuarios[index][3]
                            }),
                            $labelNombre = $("<label>", {
                                for: data.usuarios[index][3],
                                text: "Nombre"
                            }),
                            $divApellidoP = $("<div>", {
                                class: "input-field col s12 m6"
                            }),
                            $iApellidoP = $("<i>", {
                                class: "material-icons prefix",
                                text: "people"
                            }),
                            $inputApellidoP = $("<input>", {
                                value: data.usuarios[index][1],
                                type: "text",
                                id: "apellido-p" + data.usuarios[index][3]
                            }),
                            $labelApellidoP = $("<label>", {
                                for: data.usuarios[index][3],
                                text: "Apellido paterno"
                            }),
                            $divApellidoM = $("<div>", {
                                class: "input-field col s12 m6"
                            }),
                            $iApellidoM = $("<i>", {
                                class: "material-icons prefix",
                                text: "people"
                            }),
                            $inputApellidoM = $("<input>", {
                                value: data.usuarios[index][2],
                                type: "text",
                                id: "apellido-m" + data.usuarios[index][3]
                            }),
                            $labelApellidoM = $("<label>", {
                                for: data.usuarios[index][3],
                                text: "Apellido materno"
                            }),
                            $divMail = $("<div>", {
                                class: "input-field col s12 m6"
                            }),
                            $iMail = $("<i>", {
                                class: "material-icons prefix",
                                text: "mail"
                            }),
                            $inputMail = $("<input>", {
                                value: data.usuarios[index][3],
                                type: "text",
                                id: "mail" + data.usuarios[index][3]
                            }),
                            $labelMail = $("<label>", {
                                for: data.usuarios[index][3],
                                text: "Correo electrónico"
                            }),
                            $divPass = $("<div>", {
                                class: "input-field col s12 m6"
                            }),
                            $iPass = $("<i>", {
                                class: "material-icons prefix",
                                text: "vpn_key"
                            }),
                            $inputPass = $("<input>", {
                                value: data.usuarios[index][4],
                                type: "text",
                                id: "pass" + data.usuarios[index][3]
                            }),
                            $labelPass = $("<label>", {
                                for: data.usuarios[index][3],
                                text: "Contraseña"
                            }),
                            $divRowBtns = $("<div>", {
                                class: "row center"
                            }),
                            $divBtnDelete = $("<div>", {
                                class: "col s12 m4 center margin",

                            }),
                            $aDelete = $("<a>", {
                                class: "waves-effect waves-light btn red truncate",
                                text: "Eliminar",
                                name: data.usuarios[index][3],
                                onclick: "deleteUser(this.name)"
                            }),
                            $iDelete = $("<i>", {
                                class: "material-icons left",
                                text: "delete"
                            }),
                            $divBtnEdit = $("<div>", {
                                class: "col s12 m4 center margin"
                            }),
                            $aEdit = $("<a>", {
                                class: "waves-effect waves-light btn blue truncate scale-transition",
                                text: "Editar",
                                id: "edit" + data.usuarios[index][3],
                                name: data.usuarios[index][3],
                                onclick: "editUser(this.name)"
                            }),
                            $iEdit = $("<i>", {
                                class: "material-icons left",
                                text: "edit"
                            }),
                            $aCancelar = $("<a>", {
                                class: "waves-effect waves-light btn blue darken-4 truncate scale-transition hide",
                                text: "Cancelar",
                                id: "cancel" + data.usuarios[index][3],
                                name: data.usuarios[index][3],
                                value: index,
                                onclick: "cancelUser(this.name)"
                            }),
                            $iCancelar = $("<i>", {
                                class: "material-icons left",
                                text: "cancel"
                            }),
                            $divBtnGuardar = $("<div>", {
                                class: "col s12 m4 center margin"
                            }),
                            $aGuardar = $("<a>", {
                                class: "waves-effect waves-light btn green truncate scale-transition scale-out",
                                text: "Guardar",
                                id: "save" + data.usuarios[index][3],
                                name: data.usuarios[index][3],
                                onclick: "saveUser(this.name)"
                            }),
                            $iGuardar = $("<i>", {
                                class: "material-icons left",
                                text: "save"
                            });
                        if (data.usuarios[index][5] == 'Coordinador') {
                            $iColHeader.text("account_box");
                        } else if (data.usuarios[index][5] == 'Admin') {
                            $iColHeader.text("all_inclusive");
                        } else if (data.usuarios[index][5] == 'Jefe') {
                            $iColHeader.text("assignment_ind");
                        }
                        $divColHeader.append($iColHeader);
                        $divColHeader.append(data.usuarios[index][0]);
                        if (data.usuarios[index][5] == 'Coordinador') {
                            $spanColHeader.attr("data-badge-caption", data.usuarios[index][6])
                            $divColHeader.append($spanColHeader);
                            $h6Mobile.text(data.usuarios[index][6]);
                            $divMobile.append($h6Mobile);
                            $divColBody.append($divMobile);
                        }
                        $li.append($divColHeader);
                        $inputNombre.attr("disabled", "disabled");
                        $divNombre.append($iNombre);
                        $divNombre.append($inputNombre);
                        $divNombre.append($labelNombre);
                        $inputApellidoP.attr("disabled", "disabled");
                        $divApellidoP.append($iApellidoP);
                        $divApellidoP.append($inputApellidoP);
                        $divApellidoP.append($labelApellidoP);
                        $inputApellidoM.attr("disabled", "disabled");
                        $divApellidoM.append($iApellidoM);
                        $divApellidoM.append($inputApellidoM);
                        $divApellidoM.append($labelApellidoM);
                        $inputMail.attr("disabled", "disabled");
                        $divMail.append($iMail);
                        $divMail.append($inputMail);
                        $divMail.append($labelMail);
                        $inputPass.attr("disabled", "disabled");
                        $divPass.append($iPass);
                        $divPass.append($inputPass);
                        $divPass.append($labelPass);
                        $divRow.append($divNombre);
                        $divRow.append($divApellidoP);
                        $divRow.append($divApellidoM);
                        $divRow.append($divMail);
                        $divRow.append($divPass);
                        $divColBody.append($divRow);
                        $aDelete.append($iDelete);
                        $aEdit.append($iEdit);
                        $aCancelar.append($iCancelar);
                        $aGuardar.append($iGuardar);
                        $divBtnDelete.append($aDelete);
                        $divBtnEdit.append($aEdit);
                        $divBtnEdit.append($aCancelar);
                        $divBtnGuardar.append($aGuardar);
                        $divRowBtns.append($divBtnDelete);
                        $divRowBtns.append($divBtnEdit);
                        $divRowBtns.append($divBtnGuardar);
                        if (data.usuarios[index][5] != 'Admin') {
                            $divColBody.append($divRowBtns);
                        }
                        $li.append($divColBody);
                        if (data.usuarios[index][5] == 'Coordinador') {
                            $('#colap-coordi').append($li);
                        } else if (data.usuarios[index][5] == 'Admin') {
                            $('#colap-admin').append($li);
                        } else if (data.usuarios[index][5] == 'Jefe') {
                            $('#colap-jefe').append($li);
                        }
                    });
                    Materialize.updateTextFields();
                } else if (level == 'Jefe') {
                    $('#banner-jefe').removeClass("hide");
                    $('.div-nuevo-usuario').addClass("hide");
                    $('.li-nuevo-usuario').addClass("hide");
                    $('.div-consultar-usuarios').addClass("hide");
                    $('.li-consultar-usuarios').addClass("hide");
                    $('#usuario_name').append(data.nombre);                     
                        $('#usuario_name_mobile').append(data.nombre);
                    
                }
                $.each(data.programas, function (index) {
                    var $divCol = $("<div>", {
                            class: "col s12 m4 col-flex"
                        }),
                        $divCardH = $("<div>", {
                            class: "card horizontal hoverable col-flex"
                        }),
                        $divCardS = $("<div>", {
                            class: "card-stacked"
                        }),
                        $divCardC = $("<div>", {
                            class: "card-content"
                        }),
                        $blockQuote = $("<blockquote>"),
                        $h5Block = $("<h6>", {
                            text: data.programas[index][1]
                        }),
                        $divCardA = $("<div>", {
                            class: "card-action"
                        }),
                        $aCardA = $("<a>", {
                            href: "#",
                            class: "pink-text",
                            text: "Acceder",
                            id: data.programas[index][0],
                            onclick: "loadPrograma(this.id)"
                        });
                    $blockQuote.append($h5Block);
                    $divCardC.append($blockQuote);
                    $divCardA.append($aCardA);
                    $divCardS.append($divCardC);
                    $divCardS.append($divCardA);
                    $divCardH.append($divCardS);
                    $divCol.append($divCardH);
                    $("#row-programas").append($divCol);
                });
                kuonyesha(3);
            } else { //Si no se hayarón usuarios
                $('#usuario_name').append(data.nombre);
                kuonyesha(3);
                
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
            kuonyesha(3);
        }

    });
}


/*--------------------------------------------
   FUNCIÓN PARA CARGAR UN PROGRAMA ACADÉMICO
---------------------------------------------*/
function loadPrograma(fkPrograma) {
    kuonyesha(2);
    var url = "php/Consultas/create-user-access.php";
    var parametro = {
        "fk-programa": fkPrograma
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametro,
        success: function (data) {
            if (data.success) {
                location.href = "consultas.html";
            } else {
                Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                kuonyesha(3);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            Materialize.toast("¡Error!, vuelva a intentarlo", 3000);
            kuonyesha(3);
        }
    });
}

/*------------------------------------
   FUNCIÓN PARA ELIMINAR USUARIO
-------------------------------------*/
function removeToast() {
    Materialize.Toast.removeAll();
}

function deleteUser(pkUsuario) {
    pkDeleteUser = pkUsuario;
    var $toastContent = $('<span>Remover usuario..</span>');
    $toastContent.append($('<button class="btn-flat toast-action" onclick="removeToast()">Cancelar</button>'));
    $toastContent.append($('<button class="btn-flat toast-action" onclick="deleteU()">Aceptar</button>'));
    Materialize.toast($toastContent, 10000);
}

function deleteU() {
    removeToast();
    kuonyesha(2);
    var url = "php/Consultas/usuarios.php"; // El script a dónde se realizará la petición.
    var parametro = {
        "accion": "delete_usuario",
        "pk_usuario": pkDeleteUser
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametro,
        dataType: "JSON",
        success: function (data) {
            if (data.success) {
                updatePage();
                Materialize.toast("¡Exito!, usuario removido", 3000);
            } else {
                Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                kuonyesha(3);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            Materialize.toast("¡Error!, vuelva a intentarlo", 3000);
            kuonyesha(3);
        }

    });

}
/*------------------------------------
   FUNCIÓN PARA EDITAR USUARIO
-------------------------------------*/
function editUser(pkUsuario) {
    $(document.getElementById('edit' + pkUsuario)).addClass('hide');
    $(document.getElementById('cancel' + pkUsuario)).removeClass('hide');
    $(document.getElementById('save' + pkUsuario)).removeClass("scale-out");
    $(document.getElementById('save' + pkUsuario)).addClass("scale-in");

    $(document.getElementById('nombre' + pkUsuario)).removeAttr("disabled");
    $(document.getElementById('apellido-p' + pkUsuario)).removeAttr("disabled");
    $(document.getElementById('apellido-m' + pkUsuario)).removeAttr("disabled");
    $(document.getElementById('mail' + pkUsuario)).removeAttr("disabled");
    $(document.getElementById('pass' + pkUsuario)).removeAttr("disabled");
}
/*-----------------------------------------------
   FUNCIÓN PARA CANCELAR ACCIÓN EDITAR USUARIO
-----------------------------------------------*/
function cancelUser(pkUsuario) {
    $(document.getElementById('cancel' + pkUsuario)).addClass('hide');
    $(document.getElementById('edit' + pkUsuario)).removeClass('hide');
    $(document.getElementById('save' + pkUsuario)).removeClass("scale-in");
    $(document.getElementById('save' + pkUsuario)).addClass("scale-out");
    $(document.getElementById('save' + pkUsuario)).addClass("scale-out");
    var index = parseInt($(document.getElementById('cancel' + pkUsuario)).attr("value"));
    $(document.getElementById('nombre' + pkUsuario)).val(itemSaved[index][0]);
    $(document.getElementById('apellido-p' + pkUsuario)).val(itemSaved[index][1]);
    $(document.getElementById('apellido-m' + pkUsuario)).val(itemSaved[index][2]);
    $(document.getElementById('mail' + pkUsuario)).val(itemSaved[index][3]);
    $(document.getElementById('pass' + pkUsuario)).val(itemSaved[index][4]);
    $(document.getElementById('nombre' + pkUsuario)).attr("disabled", "disabled");
    $(document.getElementById('apellido-p' + pkUsuario)).attr("disabled", "disabled");
    $(document.getElementById('apellido-m' + pkUsuario)).attr("disabled", "disabled");
    $(document.getElementById('mail' + pkUsuario)).attr("disabled", "disabled");
    $(document.getElementById('pass' + pkUsuario)).attr("disabled", "disabled");
    Materialize.updateTextFields();

}
/*-------------------------------------------
   FUNCIÓN PARA GUARDAR CAMBIOS DE USUARIO
--------------------------------------------*/
function saveUser(pkUsuario) {

    var check = [];
    check = checkValidate(pkUsuario, 0);
    if (check[0] == 1) {
        Materialize.toast(check[1], 3000);
    } else {
        kuonyesha(2);
        var userinfo = [];
        userinfo[0] = $(document.getElementById('nombre' + pkUsuario)).val();
        userinfo[1] = $(document.getElementById('apellido-p' + pkUsuario)).val();
        userinfo[2] = $(document.getElementById('apellido-m' + pkUsuario)).val();
        userinfo[3] = $(document.getElementById('mail' + pkUsuario)).val();
        userinfo[4] = $(document.getElementById('pass' + pkUsuario)).val();
        var url = "php/Consultas/usuarios.php"; // El script a dónde se realizará la petición.
        var parametro = {
            "accion": "update_usuario",
            "pk_usuario": pkUsuario,
            "user-data": userinfo
        };
        $.ajax({
            type: "POST",
            url: url,
            data: parametro,
            dataType: "JSON",
            success: function (data) {
                if (data.success) {
                    updatePage();
                    Materialize.toast("¡Exito!, los cambios han sido efectuados", 3000);
                    pkDeleteUser = "null";
                } else {
                    Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                    pkDeleteUser = "null";
                    kuonyesha(3);
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                Materialize.toast("¡Error!, vuelva a intentarlo", 3000);
                pkDeleteUser = "null";
                kuonyesha(3);
            }

        });
    }
}
/*------------------------------------
   FUNCIÓN PARA ACTUALIZAR PAGÍNA
-------------------------------------*/
/*Función que actualizara el contenido de la página despues de haber realizado algún cambio o haber insertado un nuevo registro*/
function updatePage() {
    refresh = true;
    fatarat(userLevel);
    $(document).ready(function () {
    $(".li-programas").click();
});

}

/*--------------------------------------
   VALIDAR CAMPOS DE USUARIO
--------------------------------------*/
function checkValidate(pkUsuario, option) {
    var regex = "/[w-.]{2,}@([w-]{2,}.)*([w-]{2,}.)[w-]{2,4}/";
    var meta = [];
    if (option == 1 && !$('input[name=tipo]').is(':checked')) {
        meta[0] = 1;
        meta[1] = "!Alerta¡, selecione un tipo de usuario";
    } else if (option == 1 && $('input[name="tipo"]:checked').val() == 'Coordinador' && $("#program option:selected").val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, selecione un programa académico";
    } else if ($(document.getElementById('nombre' + pkUsuario)).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo nombre debe ser llenado";
    } else if ($(document.getElementById('apellido-p' + pkUsuario)).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo apellido paterno debe ser llenado";
    } else if ($(document.getElementById('apellido-m' + pkUsuario)).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo apellido materno debe ser llenado";
    } else if ($(document.getElementById('mail' + pkUsuario)).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo correo electrónico debe ser llenado";
    } else if ($(document.getElementById('mail' + pkUsuario)).val().indexOf('@', 0) == -1 || $(document.getElementById('mail' + pkUsuario)).val().indexOf('.', 0) == -1) {
        meta[0] = 1;
        meta[1] = "!Alerta¡, introduzca un correo válido";

    } else if ($(document.getElementById('pass' + pkUsuario)).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo contraseña debe ser llenado";
    } else {
        meta[0] = 0;
        meta[1] = "All right!";

    }
    return meta;
}



/*--------------------------------------
   AÑADIR USUARIO
--------------------------------------*/
$('#radioTipo2').click(function () {
    $('#div-Programa').removeClass("hide");
    $('#div-Programa').fadeIn("fast");
});
$('#radioTipo1').click(function () {
    $('#div-Programa').fadeOut("fast");
});
$('#radioTipo3').click(function () {
    $('#div-Programa').fadeOut("fast");
});
function addUser() {
    var check = [];
    check = checkValidate("-add", 0);
    if (check[0] == 1) {
        Materialize.toast(check[1], 3000);
    } else {
        kuonyesha(2);
        var userinfo = [];
        userinfo[0] = $(document.getElementById('nombre-add')).val();
        userinfo[1] = $(document.getElementById('apellido-p-add')).val();
        userinfo[2] = $(document.getElementById('apellido-m-add')).val();
        userinfo[3] = $(document.getElementById('mail-add')).val();
        userinfo[4] = $(document.getElementById('pass-add')).val();
        userinfo[5] = $('input[name="tipo"]:checked').val();
        userinfo[6] = $("#program option:selected").val();
        var url = "php/Consultas/usuarios.php"; // El script a dónde se realizará la petición.
        var parametro = {
            "accion": "add_usuario",
            "user-data": userinfo
        };
        $.ajax({
            type: "POST",
            url: url,
            data: parametro,
            dataType: "JSON",
            success: function (data) {
                if (data.success) {
                    updatePage();
                    $(document.getElementById('nombre-add')).val('');
                    $(document.getElementById('apellido-p-add')).val('');
                    $(document.getElementById('apellido-m-add')).val('');
                    $(document.getElementById('mail-add')).val('');
                    $(document.getElementById('pass-add')).val('');
                    $('#program option').prop('selected', function () {
                        return this.defaultSelected;
                    });
                    $('input[name="tipo"]').prop('checked', false);
                    Materialize.toast("¡Exito!, Usuario añadido", 3000);
                    kuonyesha(3);
                } else {
                    Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                    kuonyesha(3);
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                Materialize.toast("¡Error!, vuelva a intentarlo", 3000);
                kuonyesha(3);
            }

        });
    }
}

/*-----------------------------------
   INICIALIZAR COMPONENTES
-----------------------------------*/
function inizializzare() {
    /* inizializzare == inicializar en Italiano, función que  inicializa ciertos  componentes de algunos frameworks como
       MATERIALIZE Y WOW ANIMATION*/
    $(".nuevo-periodo").removeClass("hide");
    $(".nuevo-periodo").fadeOut(50);
    /*-----------------------------------
       INIT MODAL
    -----------------------------------*/
    $(document).ready(function () {
        // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
        $('.modal').modal();
    });

    /*-----------------------------------
       INIT WOW ANIMATION
    -----------------------------------*/
    wow = new WOW({
        boxClass: 'wow', // default
        animateClass: 'animated', // default
        offset: 0, // default
        mobile: true, // default
        live: true // default
    })
    new WOW().init();


    /*-------------------------------------
    Iniciamos smoothScroll (Scroll Suave)
    -------------------------------------*/
    smoothScroll.init({
        speed: 1000, // Integer. How fast to complete the scroll in milliseconds
        offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
    });

    /*---------------------------------
      INIT COLLAPSIBLE MATERIALIZE
  ----------------------------------*/
    $(document).ready(function () {
        $('.collapsible').collapsible();
    });


    /* ---------------------------
      INIT SELECT MATERIALIZE
    ----------------------------*/
    initializzare_select();

    /* ---------------------------
      INIT DROPDOWN MATERIALIZE
    ----------------------------*/
    initializzare_dropdown();

    /* ---------------------------
      INIT MODALES MATERIALIZE
    ----------------------------*/
    $(document).ready(function () {
        $('#modal-user-data').modal();
    });

// Initialize collapse button
  $(".button-collapse").sideNav();

    /* ---------------------------
          INIT TABS MATERIALIZE
        ----------------------------*/
    $(document).ready(function () {
        $('ul.tabs').tabs();
    });
    /*----------------------------------
      INIT PUSH SPIN MATERIALIZE
    ----------------------------------*/
    initializzare_pushpin();
}

/*----------------------------------
      INICIAR SELECT MATERIALIZE
----------------------------------*/
function initializzare_select() {
    $(document).ready(function () {
        $('select').material_select();
    });
}

/*----------------------------------
      INICIAR DROPDOWN MATERIALIZE
----------------------------------*/
function initializzare_dropdown() {
    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: false, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
    });
}


/*----------------------------------
      INICIALIZAR PUSHPIN
----------------------------------*/
function initializzare_pushpin() {
    $(document).ready(function () {
        if (screen.width > 992) {
            $('.pin-top').pushpin({
                top: 527,
                offset: 0
            });
        } else if (screen.width > 600) {
            $('.pin-top').pushpin({
                top: 510,
                offset: 0
            });
        } else {
            $('.pin-top').pushpin({
                top: 920,
                offset: 0
            });
        }
    });
}

/*--------------------------------------
    EVITAR ONCLICK POR DEFAULT EN LINK
 -------------------------------------*/
$(document).on("click", "a", function (e) {
    e.preventDefault();
})
/*--------------------------------------
TAB HECHO EN CHINA
---------------------------------------*/
$(document).ready(function () {
    $(".li-nuevo-usuario").click(function () {
        $(".div-consultar-usuarios").hide(10);
        $(".div-programas").hide(10);
        $(".div-nuevo-usuario").show("slow");
        $(".li-consultar-usuarios").removeClass("k", 1000, "easeInBack");
        $(".li-programas").removeClass("k", 1000, "easeInBack");
        $(".li-nuevo-usuario").addClass("k", 1000, "easeInBack");

    });
    $(".li-consultar-usuarios").click(function () {
        $(".div-nuevo-usuario").hide(10);
        $(".div-programas").hide(10);
        $(".div-consultar-usuarios").show("slow");
        $(".li-consultar-usuarios").addClass("k", 1000, "easeInBack");
        $(".li-nuevo-usuario").removeClass("k", 1000, "easeInBack");
        $(".li-programas").removeClass("k", 1000, "easeInBack");

    });
    $(".li-programas").click(function () {
        $(".div-nuevo-usuario").hide(10);
        $(".div-consultar-usuarios").hide(10);
        $(".div-programas").show("slow");
        $(".li-programas").addClass("k", 1000, "easeInBack");
        $(".li-nuevo-usuario").removeClass("k", 1000, "easeInBack");
        $(".li-consultar-usuarios").removeClass("k", 1000, "easeInBack");

    });
});

/*----------------------------------
      MOSTRAR U OCULTAR CONTENIDO
----------------------------------*/
function kuonyesha(almostflag) { /* kuonyesha == mostrar en Suajili, función que oculta o muestra el div de carga que tiene un gif súper cool,también activa o des activa el scroll*/
    if (almostflag == 2) {
        $(document).ready(function () {
            $('.wait').fadeIn('fast');
            $('body').css({
                'overflow': 'hidden'
            });
        });
    } else if (almostflag == 3) {
        $(document).ready(function () {
            $('.wait').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    } else if (almostflag == 4) {
        $(document).ready(function () {
            $('.overlay').removeClass("hide");
            $('.overlay').fadeIn('fast');
            $('body').css({
                'overflow': 'hidden'
            });
        });
    } else if (almostflag == 5) {
        $(document).ready(function () {
            $('.overlay').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    }
}
