<?php
//ini_set('display_errors','1');

function getConnection(){
$dsn = 'mysql:dbname=u458127246_fcnbd;host=localhost';
$user = 'u458127246_4r4c3';
$password = '4R4C31Y@BD';

    try{

    $pdo = new PDO(	$dsn, 
                        $user,
                        $password
                        );
        return $pdo;
    }catch( PDOException $e ){
        echo 'Error al conectar: ' . $e->getMessage();
    }
}


function executeQuery($Sentencia){
    try{
        $pdo=getConnection();
        $statement = $pdo->prepare($Sentencia);
        
        if($statement->execute()){
            $pdo="null";
            return $statement;  
        }else{
            echo "executeQuery :: Ocurrio un error :: ".$Sentencia;
            return false;
        }
    }catch(PDOException  $e ){
        echo "Error: ".$e;
    }
}

function executeQueryLimit($Sentencia,$offset,$limit){
    try{
        $pdo=getConnection();
        $statement = $pdo->prepare($Sentencia);
        $statement->bindValue(1, (int) $offset, PDO::PARAM_INT);
            $statement->bindValue(2, (int) $limit, PDO::PARAM_INT);
        if($statement->execute()){
            $pdo="null";
            return $statement;  
        }else{
            echo "executeQuery :: Ocurrio un error :: ".$Sentencia;
            return false;
        }
    }catch(PDOException  $e ){
        echo "Error: ".$e;
    }
}

function limpiarStm($statement){
    //Cerrar conexion
            $statement->closeCursor();
            $statement = null;
}

function executeQueryArray($Sentencia,$array){
    try{
        $pdo=getConnection();
        $statement = $pdo->prepare($Sentencia);
        
        if($statement->execute($array)){
            $pdo="null";
            return $statement;
        }else{
            echo "executeQueryArray :: Ocurrio un error :: ".$Sentencia;
            return false;
        }
    }catch(PDOException  $e ){
        echo "Error: ".$e;
    }
}

function resultQuery($Sentencia){
    try{
        $pdo=getConnection();
        $statement = $pdo->prepare($Sentencia);
        if($statement->execute()){
           $results = $statement->fetchAll();

            //Cerrar conexion
            $statement->closeCursor();
            $statement = null;
            $pdo = null;
            
            return $results; 
        }else {
            echo "resultQuery :: Ocurrio un error :: ".$Sentencia;
            $pdo = null;
            return false;
              }        
    }catch(PDOException  $e ){
    echo "Error: ".$e;
    }
}

function resultQueryJson($Sentencia){
    //Ejecutar busqueda
    if($Datos = resultQuery($Sentencia)){
        $DatosArray = array(); //creamos un array
        //guardamos en un array multidimensional todos los datos de la consulta
        $i=0;

        foreach($Datos as $row)
        {
            $DatosArray[$i] = $row;
            $i++;
        }
            $i++;
        
            $DatosArray[$i] = getCount($Sentencia);
            return json_encode($DatosArray);
    }
}

function getCount($Sentencia){
    try{
        $pdo=getConnection();
        $statement = $pdo->prepare($Sentencia);
        
        if($statement->execute()){
            $count = $statement->rowCount();
            
            //Cerrar conexion
//            $statement->closeCursor();
//            $statement = null;
//            $pdo = null;
            
            return $count;
        }else{
            echo "getCount :: Ocurrio un error :: ".$Sentencia;
            return false;
        }
    }catch(PDOException  $e ){
    echo "Error: ".$e;
    }
}

// mysqli_set_charset($conexion, "utf8"); //formato de datos utf8
//function disconnectDB($conexion){
//
//    $close = mysqli_close($conexion);
//
//    if($close){
//        echo 'La desconexión de la base de datos se ha hecho satisfactoriamente
//';
//    }else{
//        echo 'Ha sucedido un error inesperado en la desconexión de la base de datos
//';
//    }   
//
//    return $close;
//}
define(‘FORCE_SSL_LOGIN’, true);

define(‘FORCE_SSL_ADMIN’, true);
?>
