/*-----------------------------
Materializa select init
---------------------------*/
var password, password2;

$(document).ready(function () {
    $('#modal-DCB').modal();
    document.getElementById('cargando').style.display = 'none';
    document.getElementById('contenido').style.display = 'block';
    politicaAceptada();

    programaSeleccionado();

    consultarProgramas();

    password = document.getElementById('email');
    password2 = document.getElementById('email2');
    password.onchange = password2.onkeyup = passwordMatch;
});

function programaSeleccionado() {
    var p = document.getElementById('posgrado').value;
    if (p == "") {
        $("#btn-send").addClass("disabled");
    } else {
        $("#btn-send").removeClass("disabled");
    }

}

$("#posgrado").change(function () {
    programaSeleccionado();
});

function passwordMatch() {
    if (password.value !== password2.value)
        password2.setCustomValidity('Los correos electrónicos no coinciden.');
    else
        password2.setCustomValidity('');
}

function politicaAceptada() {
    politica = document.getElementById('politca').checked;
    politicaC = document.getElementById('politca');
    if (!politica) {
        politicaC.setCustomValidity('Es necesario que lea y acepte la política de privacidad para continuar.');
    } else {
        politicaC.setCustomValidity('');
    }
}


(function ($) {
    $(function () {

        $('.button-collapse').sideNav();
        $('.parallax').parallax();

    }); // end of document ready
})(jQuery); // end of jQuery name space

/*----------------------------------
Iniciamos smoothScroll (Scroll Suave)
--------------------------------*/
smoothScroll.init({
    speed: 1000, // Integer. How fast to complete the scroll in milliseconds
    offset: 0, // Integer. How far to offset the scrolling anchor location in pixels

});


/*----------------------------------
      TABS INIT
----------------------------------*/
$(document).ready(function () {
    $('ul.tabs').tabs();
});


/*----------------------------------
      FULLSCREEN SLIDER
----------------------------------*/
$(document).ready(function () {
    $('.slider').slider({
        height: 500,
        indicators: false
    });
});


/* ----------------------
MODALES INIT
------------------------*/
$(document).ready(function () {
    $('#modal-ENAFyS').modal();
});

$(document).ready(function () {
    $('#modal-ENROC').modal();
});

$(document).ready(function () {
    $('#modal-MSPAS').modal();
});

$(document).ready(function () {
    $('#modal-MCB').modal();
});
$(document).ready(function () {
    $('#modal-MGIC').modal();
});
$(document).ready(function () {
    $('#modal-MCNH').modal();
});
$(document).ready(function () {
    $('#modal-MNCI').modal();
});





/*------------------------------------
WOW ANIMATION
-----------------------------------*/
wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 0, // default
    mobile: true, // default
    live: true // default
})

new WOW().init();

/*---------------------------------
    OCULTAR Y MOSTRAR BOTON IR ARRIBA
 ----------------------------------*/
$(function () {
    $(window).scroll(function () {
        var scrolltop = $(this).scrollTop();
        if (scrolltop >= 50) {
            $(".ir-arriba").fadeIn();
        } else {
            $(".ir-arriba").fadeOut();
        }
    });

});

/*-----------------------------------
  RESET FORM
  ---------------------------------*/
function myFunction() {
    document.getElementById("pre-reg-form").reset();
}

/*-------------------------------------
    AJAX SEND DATOS
----------------------------------------*/
var limite = 1;
var current = 0;
$("#btn-send").click(function () {
    current = 0;

    $("#pre-reg-form").submit(function () {
        current++;
        if (current == 1) {
            showDialog();
            var url = "php/pre-reg.php"; // El script a dónde se realizará la petición.
            $.ajax({
                type: "POST",
                url: url,
                data: $("#pre-reg-form").serialize(), // Adjuntar los campos del formulario enviado.
                success: function (data) {
                    hideDialog();
                    if (data == '1') {
                        swal("Gracias!", "En breve recibirás un correo de confirmación!", "success", {
                            button: "Cerrar!",
                        });

                        document.getElementById("pre-reg-form").reset();
                        $('body,html').animate({
                            scrollTop: 0
                        }, 500);
                    } else if (data == '2') {
                        swal("Lo sentimos!", "Este correo ya se encuentra registrado!", "info", {
                            button: "Cerrar!",
                        });
                    } else {
                        swal("Error!", "Intentalo de nuevo!", "error", {
                            button: "Cerrar!",
                        });
                    }
                }
            });
        } else {}
        return false;
    });
});

function showDialog() {
    $("#contenido").css("display", "none");
    $("#procesando").addClass("procesando");
}

function hideDialog() {
    $("#contenido").css("display", "block");
    $("#procesando").removeClass("procesando");
}

function consultarProgramas() {
    showDialog();
    var url = "php/php_index.php?funcion=consultarProgramas"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: $("#pre-reg-form").serialize(), // Adjuntar los campos del formulario enviado.
        success: function (data) {
            //            $("#posgrado").append("<option selected disabled></option>");
            if (data != "") {
                var programas = JSON.parse(data);
                console.log(programas);

                $.each(programas, function (index) {
                    var nombre = programas[index].nombre;
                    var pk = programas[index].pk_programa;

                    if (nombre != undefined) {
                        $("#posgrado").append("<option value=" + pk + ">" + nombre + "</option>");
                        console.log(nombre + ":" + pk + ":" + " agregado");
                    }

                });

                $(document).ready(function () {
                    $('select').material_select();
                });

            }
            hideDialog();
        }
    });
}
