<?php

//conexion
include("conect.php");

//Generar clave aleatoria
function generarClave($length) { 
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length); 
}
 
//Obtenci&oacute;n de datos
    $Nombre = addslashes(htmlspecialchars($_POST["nombre"]));
    $ApellidoP = addslashes(htmlspecialchars($_POST["apellido_p"]));
    $ApellidoM = addslashes(htmlspecialchars($_POST["apellido_m"]));
    $Correo = addslashes(htmlspecialchars($_POST["correo"]));
    $Carrera = addslashes(htmlspecialchars($_POST["posgrado"]));

//Obtener Matricula
// Establecer la zona horaria predeterminada a usar. Disponible desde PHP 5.1
    date_default_timezone_set('UTC');
    $año=date("Y");
    $fechaActual=date("Y-m-d");
    
    //Silgas
        $sql="select nombre,siglas from programas where pk_programa=".$Carrera."";
        $result=resultQuery($sql);
        $Siglas=$result[0]['siglas'];
        $NombreCarrera=$result[0]['nombre'];

        //periodo
        $sql = "select pk_periodo,periodo_año from periodos where fk_programa='".$Carrera."' and '".$fechaActual."' between fecha_inicio and fecha_fin";
//        $sql=utf8_decode($sql);
        $result=resultQuery($sql);
        $periodo=$result[0][1];
        $PKperiodo=$result[0]['pk_periodo'];
 
        //alumnos
        $sql="select pk_matricula from alumnos where fk_periodo='".$PKperiodo."';";
        $count=getCount($sql);
    //    $count=685; 

        //espacio disponible
        $space="000";
        if($count >= 10){
            $space="00";
        }
        if ($count >= 100){
            $space="0";
        }
        if ($count >= 1000){
            $space="";
        }

//        $Matricula = $Siglas.$año.$periodo.$space.$count;

        //Periodo
        $Periodo=$PKperiodo;
        
        $Contraseña=generarClave(6);



//Envio de Correo
// Varios destinatarios
$para  = $Correo;// . ', '; // atenci&oacute;n a la coma
//$para .= 'wilfred.ironman@gmail.com';

// titulo
$titulo = 'Facultad de Ciencias Naturales';

// mensaje
$mensaje = '<html>
<head>
    <title>Proceso de admisi&oacute;n</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
    <table style="width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;">
        <tr>
            <td style="text-align: center">
                <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/icouaq.png" width="100px" height="100px">
            </td>
            <td >
                <h2  style="text-align: center; color: #3498db; margin: 0 0 12px; font-size: 30px; font-family: inherit">Proceso de admisi&oacute;n.</h2>
                <h4 style="text-align: center; color: #3498db; margin: 0 0 12px; font-size: 20px; font-family: inherit">'.$NombreCarrera.'</h4>
            </td>
            <td style="text-align: center">
                <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/icofc.png" width="100px" height="100px">
            </td>
        </tr>
    </table>
    <br>
    <table style="width: 600px; margin:0 auto; border-collapse: collapse;" >

        <tr>
            <td style="background-color: #ecf0f1">
                <div style="color: #34495e; margin: 4% 10% 2%; text-align: justify;font-family: sans-serif">
                    <h3 style="color: #e67e22; margin: 0 0 7px">Hola '.$Nombre.' '.$ApellidoP.' '.$ApellidoM.'</h3>
                    <p style="margin: 2px; font-size: 15px">
                        Su pre-registro ha sido completado con &eacute;xito, solicitamos su confirmaci&oacute;n antes que pueda contestar los cuestionarios y adjuntar los documentos necesarios para continuar con el proceso de admisi&oacute;n a '.$NombreCarrera.'.<br>
                    </p>
                    <ul style="font-size: 14px;  margin: 10px 0">
                        <li>Sus datos no ser&aacute;n divulgados por ning&uacute;n motivo.</li>
                        <li>Se le notificará por este medio los resultados de su postulaci&oacute;n.</li>
                        <li>Deber&aacute; contar con todos los documentos solicitados en formato PDF.</li>
                        <li>La institución podrá evaluar su registro hasta que usted haya proporcionado toda la información solicitada.</li>
                    </ul>
                    <hr>
                    Datos de acceso.<br>
                    <div style="font-size: 12px;  margin: 5px 0">
                    Usuario: '.$Correo.'<br>
                    Contrase&ntilde;a: '.$Contraseña.'<br>
                    </div>
                  <br>
                    <div style="text-align: center">
                     <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/'.$Siglas.'.png" width="200px" height="200px">
                    </div> 
                    <br clear="all">
                    <div style="width: 100%; text-align: center">
                        <a style="text-decoration: none; border-radius: 5px; padding: 11px 23px; color: white; background-color: #3498db" href="http://aspirantes.posgradosfcn-uaq.com/login.php?n='.$Nombre.'&ap='.$ApellidoP.'&am='.$ApellidoM.'&c='.$Correo.'&p='.$Carrera.'&pa='.$Contraseña.'">Continuar con el proceso</a>
                    </div>
                    <p style="color: #b3b3b3; font-size: 12px; text-align: center;margin: 30px 0 0">UAQ, Facultad de Ciencias Naturales.</p>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>';

// Para enviar un correo HTML, debe establecerse la cabecera Content-type
$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

// Cabeceras adicionales
$cabeceras .= 'To:'.$Nombre.'<'.$Correo.'>'."\r\n";
$cabeceras .= 'From: Facultad de Ciencias Naturales <fcn@posgradosfcn-uaq.com>' . "\r\n";

//Registrar Aspirante
$boolley=3;

//Variable de verificacion de correo
$verifycorreo=0;
        $sql = "SELECT correo FROM alumnos;";
       $results = executeQuery($sql)->fetchAll();

          foreach($results as $rs)
          {
             if($rs['correo'] == $Correo){
                 $verifycorreo=1;//Correo repetido
                 $boolley=2;//Mensaje de correo repetido
             }
         }

//Si el correo no se repite
    if($verifycorreo==0){

//               $query="INSERT INTO alumnos (pk_matricula,fk_periodo,estado,fk_programa,nombre,apellido_p,apellido_m,correo,password) VALUES(?,?,?,?,?,?,?,?,?)";
//        
//               executeQueryArray($query,array($Matricula,$Periodo,"Sin Confirmar",$Carrera,$Nombre,$ApellidoP,$ApellidoM,$Correo,$Matricula));
//
//                $query2="insert into apartados (fk_matricula,cuestionario,documentos,referencias) values (?,?,?,?);";
//                executeQueryArray($query2,array($Matricula,"0","0","0"));
                
    
//                $mensaje=str_replace($search, $replace, $mensaje);
        
                // Enviarlo
                $enviado = mail($para, $titulo, $mensaje, $cabeceras);
                if($enviado){
                       $boolley = 1;
                }else{
                         $boolley = 0;
                }
            }


//echo $boolley.': /login.php?n='.$Nombre.'&ap='.$ApellidoP.'&am='.$ApellidoM.'&c='.$Correo.'&p='.$Carrera.'&pa='.$Contraseña;

echo $boolley;
?>