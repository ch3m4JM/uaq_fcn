<?php
    //inicio de sesión
    session_start();
    
    //GET
    $funcion=$_GET['funcion'];

    //Switch
    switch($funcion){
        case 'aspirante':
        getDatosAspirante();
        break;
        
        case 'apartados':
        getDatosApartados();
        break;
            
        case 'cambiarContraseña':
            cambiarContraseña();
            break;
            
        case 'diasRestantes':
            diasRestantes();
            break;
            
        case 'apartadosCompletos':
            apartadosCompletos();
            break;
        
        default:
            echo "La funcion que intenta llamar no existe: ".$funcion;
            break;
    }

   function getDatosAspirante(){
        if (isset($_SESSION["pk"])){
                //conexión
                include("../../php/conect.php");
                
                //matricula
                $matricula=$_SESSION["pk"];
                
                //consulta de datos
                $sqlDatos="select alumnos.nombre,alumnos.estado,alumnos.correo,programas.siglas,programas.nombre from alumnos INNER join programas on (alumnos.fk_programa=programas.pk_programa) where alumnos.pk_matricula='".$matricula."';";
                
                echo resultQueryJson($sqlDatos);
    
            }else{
                echo "salir";
            }
    }

    function getDatosApartados(){
        if (isset($_SESSION["pk"])){
                //conexión
                include("../../php/conect.php");
            
                //matricula
                $matricula=$_SESSION["pk"];
            
                //consulta de apartados
                $sqlApartados="select * from apartados where fk_matricula='".$matricula."';";
                
                echo resultQueryJson($sqlApartados);
        }else{
                echo "salir";
            }
    }

    function cambiarContraseña(){
        if (isset($_SESSION["pk"])){
                //conexión
                include("../../php/conect.php");
            
                //matricula
                $matricula=$_SESSION["pk"];
                
                //Obtener Datos del formulario
                $Contraseña=$_POST['inputContraseñaAntigua'];
                $ContraseñaNueva=$_POST['inputContraseñaNueva'];
            
                //Verificar que la contraseña actual sea correcta
                $SQL="Select password from alumnos where pk_matricula='".$matricula."' and password='".$Contraseña."';";
                $Coincidencia=getCount($SQL);
            
                //Si la contraseña actual coincide
                if($Coincidencia == 1){
                    //Cambiar Contraseña
                    $SQL1="update alumnos set password='".$ContraseñaNueva."' where pk_matricula='".$matricula."';";
                    if($rs = executeQuery($SQL1)){
                        limpiarStm($rs);
                        echo "true";
                    }else echo "error";
                }else{
                    echo "false";
                }
            
        }else{
                echo "salir";
            }
    }
    
    function diasRestantes() {
        if (isset($_SESSION["pk"])){
                //conexión
                include("../../php/conect.php");
            
                //matricula
                $Matricula=$_SESSION["pk"];
                
                //Obtener fecha actual
                // Establecer la zona horaria predeterminada a usar. Disponible desde PHP 5.1
                date_default_timezone_set('UTC');
                $fecha_actual = date("Y-m-d");
            
                //Obtener fecha limite
                $sql="SELECT fecha_fin from periodos inner join alumnos on (alumnos.fk_periodo = periodos.pk_periodo) where alumnos.pk_matricula='".$Matricula."';";
                $result=resultQuery($sql);
                $fecha_limite=$result[0]['fecha_fin'];
            
                //Calcular dias restantes
                $s = strtotime($fecha_limite)-strtotime($fecha_actual);  
                $d = intval($s/86400);  
                $diasRestantes = $d;
                
                //Devolver fecha limite y dias restantes
                $Datos[0]["fecha_limite"]=$fecha_limite;
                $Datos[0]["dias_restantes"]=$diasRestantes;
                echo json_encode($Datos);  
        }else{
                echo "salir";
            }
    } 

    function apartadosCompletos(){
        if (isset($_SESSION["pk"])){
                //conexión
                include("../../php/conect.php");
            
                //matricula
                $Matricula=$_SESSION["pk"];
            
                //cambiar de estado
                $sql="update alumnos set estado='En espera' where pk_matricula='".$Matricula."';";
                if($rs=executeQuery($sql)){
                    limpiarStm($rs);
                    echo "echo";
//                    echo $sql;
                }else echo "error";
            }else{
                echo "salir";
            }
    }
              
?>
