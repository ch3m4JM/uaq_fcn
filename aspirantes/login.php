<?php
//Si se recibieron todos los parametros
if(isset($_GET['n']) && isset($_GET['ap']) && isset($_GET['am']) && isset($_GET['c']) && isset($_GET['p'])){
    //conexion
    include("php/conect.php");
    
    //Obtención de datos
    $Nombre = addslashes(htmlspecialchars($_GET["n"]));
    $ApellidoP = addslashes(htmlspecialchars($_GET["ap"]));
    $ApellidoM = addslashes(htmlspecialchars($_GET["am"]));
    $Correo = addslashes(htmlspecialchars($_GET["c"]));
    $Carrera = addslashes(htmlspecialchars($_GET["p"]));
    $Contraseña = addslashes(htmlspecialchars($_GET["pa"]));
    
    
    //Se obtiene la fecha actual
    // Establecer la zona horaria predeterminada a usar. Disponible desde PHP 5.1
    date_default_timezone_set('UTC');
    $año=date("Y");
    $fechaActual=date("Y-m-d");
    
    //Verificar si hay periodos activos y la informacion es correcta
    $sql = "SELECT * FROM periodos where fk_programa='".$Carrera."' and '".$fechaActual."' between fecha_inicio and fecha_fin;";
    
    //Verificar si ya se ha validado este correo
    $sql2="Select * from alumnos where correo='".$Correo."';";
    
    //Si el periodo se encuetra activo
    if(getCount($sql) > 0 && $co = getCount($sql2) < 1){

        //Silgas
        $sql="select nombre,siglas from programas where pk_programa='".$Carrera."';";
        $result=resultQuery($sql);
        $Siglas=$result[0]['siglas'];

        //periodo
        $sql = "SELECT pk_periodo,periodo_año FROM periodos where fk_programa='".$Carrera."' and '".$fechaActual."' between fecha_inicio and fecha_fin;";
        $result=resultQuery($sql);
        $periodo=$result[0]['periodo_año'];
        $PKperiodo=$result[0]['pk_periodo'];

        //alumnos
        $sql="select pk_matricula from alumnos where fk_periodo='".$PKperiodo."';";
        $count=getCount($sql)+1;
    //    $count=685; 

        //espacio disponible
        $space="000";
        if($count >= 10){
            $space="00";
        }
        if ($count >= 100){
            $space="0";
        }
        if ($count >= 1000){
            $space="";
        }

        $Matricula = $Siglas.$año.$periodo.$space.$count;

        //Periodo
        $Periodo=$PKperiodo;

        //Se hace el registro del aspirante
        $query="INSERT INTO alumnos (pk_matricula,fk_periodo,estado,fk_programa,nombre,apellido_p,apellido_m,correo,password) VALUES(?,?,?,?,?,?,?,?,?)";
        
        
        //Si se registro al aspirante
        if($s = executeQueryArray($query,array($Matricula,$Periodo,"En proceso",$Carrera,$Nombre,$ApellidoP,$ApellidoM,$Correo,$Contraseña))){
            limpiarStm($s);
            //Se crean los apartados
            $query2="insert into apartados (fk_matricula,cuestionario,documentos,referencias) values (?,?,?,?);";
            if($s1 = executeQueryArray($query2,array($Matricula,"0","0","0"))){
                limpiarStm($s1);
                //Se registra en documentos
                $query="insert into documentos (fk_matricula) values(?)";
                if($s2 = executeQueryArray($query,array($Matricula))){
                    limpiarStm($s2);
                    $query3="insert into recomendaciones (fk_matricula,fk_referencia,estado) values (?,?,?);";
                    if($s3 = executeQueryArray($query3,array($Matricula,1,"Sin enviar")) && $s4 = executeQueryArray($query3,array($Matricula,2,"Sin enviar"))){
//                        limpiarStm($s3);
//                        limpiarStm($s4);
                        //Variables de confirmacion
                        $confirmacion="correcto";
                        $mensaje=$Correo;
                    }else{
                        //Variables de confirmacion
                        $confirmacion="incorrecto";
                        $mensaje="No se registro en referencias."; 
                    }
                }else {
                    //Variables de confirmacion
                    $confirmacion="incorrecto";
                    $mensaje="No se registro en documentos.";
                }
                
            }else{
            //Variables de confirmacion
            $confirmacion="incorrecto";
            $mensaje="No se crearon los apartados.";
            }
        }else{
            //Variables de confirmacion
            $confirmacion="incorrecto";
            $mensaje="No se registro al aspirante.";
        }
    }else {
        //Variables de confirmacion
        
        if ($co = 1) {
            $mensaje="Su correo ya ha sido validado anteriormente y puede ingresar sin ningun problema.";
            $confirmacion="correcto2";
        }
        else {
            $mensaje="El periodo de aceptación de aspirantes a expirado.";
            $confirmacion="incorrecto";
        }
    }

}

?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <title>LOGIN - Aspirantes a posgrado - FCN - UAQ</title>
        <meta name="author" content="Posgrados Facultad de Ciencias Naturales">
        <meta name="classification" content="Registro de aspirantes a la facultad de Ciencias Naturales">
        <meta name="description" content="Registro de aspirantes a posgrado de la Facultad de Ciencias Naturales - UAQ">
        <meta name="generator" content="Brackets">
 
        <!-- Google Fonts -->
        <!--        <link href="cuestionarios/css/bootstrapV4.css" rel="stylesheet">-->
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="css/style_lg.css">
        <link rel="stylesheet" href="css/animate_lg.css">
        <!-- Custom Stylesheet -->
        <!--    <link rel="stylesheet" href="css/bootstrapV4.css">-->
    </head>

    <body>
        <div id="procesando"></div>
        <div id="cargando"></div>
        <div class="container" id="arriba">
            <div class="top">
                <div>
                    <img src="img/UAQWHITE.png" class="imagen animated fadeInUp">
                </div>
                <div>
                    <img src="img/logos/icouaq.png" class="imagen2 animated fadeInUp">
                </div>
                <div class="top-content">
                    <h1 id="title" class="hidden"><span id="logo">Facultad de  Ciencias Naturales</span></h1>
                </div>
            </div>

            <!--            formulario de ingreso-->
            <form id="login-form">
                <div class="login-box animated fadeInUp">
                    <div class="box-header">
                        <h2>Ingresar</h2>
                    </div>
                    <label for="username">Correo</label>
                    <br/>
                    <input type="text" id="username" name="username" required>
                    <br/>
                    <label for="password">Contraseña</label>
                    <br/>
                    <input type="password" id="password" name="password" required>

                    <div id="informacion" style="text-align : justify;">

                        <?php
                        if(isset($confirmacion) && isset($mensaje)){
                            echo "<input type='hidden' id='confirmacion' name='confirmacion' value='".$confirmacion."'>";
                            echo "<input type='hidden' id='mensaje' name='mensaje' value='".$mensaje."'>";
                        };
                    ?>
                    </div>

                    <button type="submit" id="btn-send-login">Entrar</button>

                    <a href="#" onclick="showRecuperar()">
                        <p class="small">Recuperar contraseña</p>
                    </a>
                    <a href="./">
                        <p class="small">¿Aún no has echo tu pre-registro?</p>
                    </a>
                </div>
                <br>
            </form>

            <!--            formulario de recuperacion de contraseña-->
            <form id="recover-form" class="hide">
                <div class="login-box animated fadeInUp">
                    <div class="box-header">
                        <h2>Recuperar contraseña</h2>
                    </div>
                    <label for="correoRecuperacion">Correo</label>
                    <br/>
                    <input type="email" id="correoRecuperacion" name="correoRecuperacion" required>
                    <br/>
                    <label for="confirmarCorreo">Confirme el correo</label>
                    <br/>
                    <input type="email" id="confirmarCorreo" name="confirmarCorreo" required>
                    <div>
                        <p>Le enviaremos un correo<br>con su contraseña.</p>
                    </div>
                    <button type="submit" id="btn-send-recover">Recuperar contraseña</button>

                    <a href="#" onclick="showLogin()">
                        <p class="small">Ingresar</p>
                    </a>
                    <a href="./">
                        <p class="small">¿Aún no has echo tu pre-registro?</p>
                    </a>
                </div>
                <br>
            </form>

        </div>
    </body>

    <script src="js/jquery.js"></script>
    <script src="js/sweetalert.min.js"></script>
    <script src="js/Custom/login.js"></script>

    </html>
