<?php

/*---------------------------------------
 DEEP MINT WAS HERE <(°_°)>
---------------------------------------*/

include('../conect.php');

//Reanuda la sesión y se traen las variables que hay en ella 
    session_start();
    $correo = $_SESSION['pk'];
    $tipo = $_SESSION['tipo'];
    $programa = $_SESSION['fk'];
    $nombre = $_SESSION['nombre'];
//Array que enviara todos los datos al JS con AJAX
$jsondata = array();

//Ejecutamos la función choose y le enviamos la acción que solicitamos
choose($_POST['accion']);

/*--------------------------------------
     FUNCIÓN ELEJIR ACCIÓN
--------------------------------------*/
//Función para elejir la acción requerida y así devolver el json correspondiente
function choose($accion){
     global $jsondata;
    switch ($accion) {
                case "consultar-periodos":
                     consulta_Inicial();
                    break;
                case "reporte":
                    reporte($_POST['fk-periodo']);
                    break;
            
                
                }
     //Se envián los datos al JS
  header('Content-type: application/json; charset=utf-8');
  echo json_encode($jsondata, JSON_FORCE_OBJECT);
}


/*-----------------------------------------
   FUNCIÓN CONSULTA INICIAL
-----------------------------------------*/
function consulta_Inicial(){
 global $jsondata, $programa, $nombre;

    //Nombre del usuario
    $jsondata["nombre"] = $nombre; 
/*-----------------------------------------
   CONSULTAR NOMBRE DEL PROGRAMA ACADÉMICO
-----------------------------------------*/
//Consulta el nombre del programa al que pertenece el usuario
$QueryOne = "SELECT nombre FROM programas WHERE pk_programa = '$programa'";
$resultsOne = executeQuery($QueryOne)->fetchAll();
 foreach($resultsOne as $rs) {
     $jsondata["programa"] = $rs['nombre']; 
 }


/*--------------------------------------
   CONSULTAR PERIODOS
--------------------------------------*/   
    $squareOne=0;
    $squareTwo=0;
    $rowMaster =array();
    $rowChild = array();
//Consulta de los periodos correspondientes al programa del usuario      
    $QueryThree = "SELECT DISTINCT año FROM periodos WHERE fk_programa = '$programa' ORDER BY año DESC";
if ($resultsThree = executeQuery($QueryThree)->fetchAll()){    
    $jsondata["periodos"] = array();
    foreach($resultsThree as $rsThree) {
    $squareOne=0;
    $year =$rsThree['año'];
    $QueryTwo = "SELECT pk_periodo, fecha_inicio, fecha_fin, periodo_año FROM periodos WHERE fk_programa = '$programa' AND año='$year' ORDER BY  periodo_año ASC";
    if ($resultsTwo = executeQuery($QueryTwo)->fetchAll()){
    $jsondata["success"] = true;
     foreach($resultsTwo as $rs) {
         $row = array();
         $row[0] = $rs['pk_periodo'];
         $row[1] = armarTextoFecha2($rs['fecha_inicio'],$rs['fecha_fin']);
         $row[2] = $rs['periodo_año'];
         $row[3] = $year;
         $row[4] = periodoActivo($rs['pk_periodo']);
        $rowChild[$squareOne]=$row;
            $squareOne++;
     } 
        $rowMaster[$squareTwo]=$rowChild;
        $squareTwo++;
    }else
    {
       $jsondata["success"] = false;
    }
        
    }    
    $jsondata["periodos"] = $rowMaster;
}else{
    $jsondata["success"] = false;
}

 
     
}

/*--------------------------------------
   FUNCIÓN ARMAR TEXTO PERIODO 2
--------------------------------------*/
//Función para armar el texto del tipo "Periodo Enero / Febrero 2017" a travéz de una fecha de inicial y una final
function armarTextoFecha2($fechaUno,$fechaDos){    
 $fecha_Inicio = explode('-',$fechaUno);
 $fecha_Fin = explode('-',$fechaDos);  
    
//Se obtienen los nombres de los meses utilizando la función sacarMes
 $mes_Inicio = sacarMes($fecha_Inicio[1]);
 $mes_Fin = sacarMes($fecha_Fin[1]);
 
//Se arma el texto del periodo

     $textoFecha = $fecha_Inicio[2]." ".$mes_Inicio." ".$fecha_Inicio[0]." hasta ".$fecha_Fin[2]." ".$mes_Fin." ".$fecha_Fin[0];
 
    return $textoFecha;    
}

/*--------------------------------------
     FUNCIÓN OBTENER NOMBRE DEL MES
--------------------------------------*/
//Función para obtener el nombre del mes con el número
function sacarMes($mesNumber){
    $textMes="Any";
    switch ($mesNumber) {
                case "1":
                     $textMes = "Enero";
                    break;
                case "2":
                     $textMes = "Febrero";
                    break;
                case "3":
                     $textMes = "Marzo";
                    break;
                case "4":
                     $textMes = "Abril";
                    break;
                case "5":
                     $textMes = "Mayo";
                    break;
                case "6":
                     $textMes = "Junio";
                    break;
                case "7":
                     $textMes = "Julio";
                    break;
                case "8":
                     $textMes = "Agosto";
                    break;
                case "9":
                     $textMes = "Septiembre";
                    break;
                case "10":
                     $textMes = "Octubre";
                    break;
                case "11":
                     $textMes = "Noviembre";
                    break;
                case "12":
                     $textMes = "Diciembre";
                    break;
                
                }
    return $textMes;
}



/*--------------------------------------
   FUNCIÓN PERIODO ACTIVO
--------------------------------------*/  
/*función para determinar si un periodo esta activo retornando un valor distinto para cada situación, "before": periodo aún no vigente, "after": periodo ya pasado, "just_now": periodo activo justo ahora*/
function periodoActivo($id_periodo){
    global $jsondata;
$yes_no_maybe="I don´t now, can you repeat the question?";
/*----------------------------------------------------------------
   CONSULTAR FECHA FINAL DE PERIODO
----------------------------------------------------------------*/ 
$fecha_inicio;
$fecha_fin;
$fecha_actual;
$QueryOne = "SELECT fecha_inicio, fecha_fin FROM periodos WHERE pk_periodo = '$id_periodo'";
if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
     foreach($resultsOne as $rs) {
         $fecha_inicio = $rs['fecha_inicio'];
         $fecha_fin = $rs['fecha_fin'];
     } 
    
    //Limpiamos el statement de memoria
   // limpiarStm($resultsTwo);
    
        /* Se compara la fecha actual y la fecha final y de inicio del periodo para ver si esta habilitado */
        
    date_default_timezone_set('America/Mexico_City');

$fecha_actual = date("Y-m-d");

        if($fecha_actual < $fecha_inicio){
                $yes_no_maybe = "before";
            $jsondata["date"] = $fecha_actual." before"; 
        }else if($fecha_actual > $fecha_fin){
                $yes_no_maybe = "after";
           $jsondata["date"] = $fecha_actual." after";
        }else{
            $yes_no_maybe = "just_now";
            $jsondata["date"] = $fecha_actual." just_now";
        }
}else
{
   $yes_no_maybe="error";
}    
    return $yes_no_maybe;
}


/*--------------------------------------
   FUNCIÓN REPORTE
--------------------------------------*/
function armarReporte($periodo){
     global $jsondata, $programa; //JSON QUE DEVUELVE TODO Y FK DEL PROGRAMA, ID PERIODO RECIBIDA EN LA FUNCIÓN armarReporte($periodo)
    
    $jsondata["success"] = true; //Devuelve este en true si todo ha ido bien
    //Llena estos arrays con los datos que necesitas para cada gráfica
    $jsondata["gráfico-h-m"] = array();
    $jsondata["gráfico-nácional-inter"] = array();
    $jsondata["gráfico-uaq-nouaq"] = array();
    $jsondata["gráfico-aceptado-noaceptado"] = array();
    $jsondata["gráfico-tiempo-parcial-completo"] = array();
}

 ?>
