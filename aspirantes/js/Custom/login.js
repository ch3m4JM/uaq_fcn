$(document).ready(function () {
    $('#logo').addClass('animated fadeInDown');
    $("input:text:visible:first").focus();
    document.getElementById('cargando').style.display = 'none';

    password = document.getElementById('correoRecuperacion');
    password2 = document.getElementById('confirmarCorreo');
    password.onchange = password2.onkeyup = passwordMatch;
});

function passwordMatch() {
    if (password.value !== password2.value) {
        password2.setCustomValidity('Los correos electrónicos no coinciden.');
        $("#confirmarCorreo").addClass("invalid");

    } else {
        password2.setCustomValidity('');
        $("#confirmarCorreo").removeClass("invalid");
    }
}

$('#username').focus(function () {
    $('label[for="username"]').addClass('selected');
});
$('#username').blur(function () {
    $('label[for="username"]').removeClass('selected');
});
$('#password').focus(function () {
    $('label[for="password"]').addClass('selected');
});
$('#password').blur(function () {
    $('label[for="password"]').removeClass('selected');
});

$(document).ready(function () {
    var $confirmacion = $("#confirmacion").val();
    if ($confirmacion != null) {
        var $mensaje = $("#mensaje").val();
        switch ($confirmacion) {
            case 'correcto':
                swal("¡Verificado!", "Ahora puede acceder para continuar con el proceso de selección ingresando los datos de acceso que se enviaron a " + $mensaje, "success", {
                    button: "Continuar",
                });
                break;
            case 'incorrecto':
                swal("Lo sentimos", $mensaje, "info", {
                    button: "Aceptar",
                });
                break;
            case 'correcto2':
                swal("Información", $mensaje, "info", {
                    button: "Aceptar",
                });
                break;
        }

    }

});

$("#btn-send-login").click(function () {

    $("#login-form").submit(function () {
        showDialog();
        var url = "php/login-user-type.php"; // El script a dónde se realizará la petición.
        $.ajax({
            type: "POST",
            url: url,
            data: $("#login-form").serialize(), // Adjuntar los campos del formulario enviado.
            success: function (data) { //data trae la URL correspondiente desde el echo que recibe del var url
                if (data == '0') { //Si el data trae un 0 como respuesta los datos de accesso son incorrectos
                    hideDialog();
                    swal("Error!", "Correo o contraseña incorrectos!", "error", {
                        button: "Cerrar!",
                    });
                } else {
                    location.href = data;
                }
            }
        });
        return false;
    });
});

function showDialog() {
    $(".container").css("display", "none");
    $("#procesando").addClass("procesando");
}

function hideDialog() {
    $(".container").css("display", "block");
    $("#procesando").removeClass("procesando");
}

function showRecuperar() {
    $("#login-form").hide(1000);

    if ($("#recover-form").hasClass("hide")) {
        $("#recover-form").removeClass("hide", 1000);
    }

    $("#recover-form").show(1000);
}

function showLogin() {
    $("#recover-form").hide(1000);
    $("#login-form").show(1000);
}

$("#recover-form").submit(function () {
    showDialog();
    var url = "php/recoverPass.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: $("#recover-form").serialize(), // Adjuntar los campos del formulario enviado.
        success: function (data) { //data trae la URL correspondiente desde el echo que recibe del var url
            if (data == "Hecho") {
                swal("Contraseña enviada", "Hemos enviado a su correo sus datos de acceso.", "success", {
                    button: "Cerrar!",
                }).then((value) => {
                    javascript: window.location.reload();
                });
            } else if (data == "Error") {
                swal("Ha ocurrido un error", "Sentimos el inconveniente, por favor recargue la pagina e inténtelo de nuevo.", "error", {
                    button: "Cerrar!",
                }).then((value) => {
                    javascript: window.location.reload();
                });
            } else if (data == "No registrado") {
                swal("Usuario no registrado", "No se ha encontrado ninguna coincidencia con el correo que ingreso, por favor verifique que este bien escrito e inténtelo de nuevo.", "info", {
                    button: "Cerrar!",
                });
            }
            hideDialog();
        }
    });
    return false;
});
