/*---------------------------------------
       DEEP MINT WAS HERE  <(°_°)> 
---------------------------------------*/
//VARIABLES GLOBALES
var referencias = 0; //Se utiliza para enviar un TOAST en caso de que aún no haya referencias de los aspirantes
var ponderaciones = 0; //Se utiliza para enviar un TOAST en caso de que aún no haya ponderaciones en el programa de los aspirantes
var slidersName = []; //Se guardan los nombres de los sliders para relacionarlos con los campos de las tabla ponderaciones/ponderacion_aspirante
var slidersFk = []; //Se guardan los sliders para poder manipularlos
var slidersValues = []; //Se guarda el valor inical de los sliders para resetearlos si se desea.
/*---------------------------------------
            VALIDAR SESIÓN 
---------------------------------------*/
function Kakunin() { /* Kakunin == Validar en japonés --- función que valida la sesión y hace que el resto de la pagina continue*/

    //kuonyesha(2);
    var url = "php/Consultas/validar-sesion.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        success: function (data) { /* Data contendra el valor true encaso de que se valide la sesión o nanay en caso contrario a travéz del archivo php validar-sesion */
            if (data == 'true') { //Sí hay sesión ejecuta las demás funciones
                inizializzare();
                loadEveryThing();
            } else if (data == 'nanay') { //Si no hay sesión nos vamos al login
                //De regreso al login
                location.href = "login.php";
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error nos vamos al login
            //De regreso al login
            location.href = "login.php";
        }

    });
}
Kakunin(); //Ejecutamos la función Kakunin


/*---------------------------------------
            CARGAR PERIODOS y NOMBRE
---------------------------------------*/
function loadEveryThing() { /* Función encargada de llenar todos los datos correspondientes al aspirante*/
    var url = "php/Consultas/consulta-aspirante.php"; // El script a dónde se realizará la petición.
    var parametro = {
        "accion": "consulta-inicial"
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametro,
        dataType: "JSON",
        success: function (data) {
            if (data.success) {
                var $a = $('<a>', {
                    text: "Salir",
                    onclick: "loadPage()"
                });
                $('#link-inicio').append($a);
                $('#usuario_name').append(data.nombre);

                $('#programa_name').text(
                    "Aspirante a " + data.programa
                );
                $('#accept-name').text("¡Aceptar a " + data.aspirante['nombre'] + "!")
                $('#dat-apellido-p').attr('value', data.aspirante['apellido_p']);
                $('#dat-apellido-m').attr('value', data.aspirante['apellido_m']);
                $('#dat-matricula').text(
                    data.aspirante['pk_matricula']
                );
                $('#dat-estado').text(
                    data.aspirante['estado']
                );
                $('#dat-nombre').text(data.aspirante['nombre']);
                $('#dat-nombre-2').attr('value', data.aspirante['nombre']);
                if (data.aspirante['sexo'] == 'Masculino') {
                    $('#dat-sexo-h').attr('checked', "checked");
                    $('#dat-sexo-m').attr('disabled', "disabled");
                } else if (data.aspirante['sexo'] == 'Femenino') {
                    $('#dat-sexo-m').attr('checked', "checked");
                    $('#dat-sexo-h').attr('disabled', "disabled");
                } else {
                    $('#dat-sexo-m').attr('disabled', "disabled");
                    $('#dat-sexo-h').attr('disabled', "disabled");
                }
                if (data.aspirante['trabaja'] == 'Si') {
                    $('#dat-empleo-si').attr('checked', "checked");
                    $('#dat-empleo-no').attr('disabled', "disabled");

                } else if (data.aspirante['trabaja'] == 'No') {
                    $('#dat-empleo-no').attr('checked', "checked");
                    $('#dat-empleo-si').attr('disabled', "disabled");
                } else {
                    $('#dat-empleo-no').attr('disabled', "disabled");
                    $('#dat-empleo-si').attr('disabled', "disabled");
                }
                if (data.aspirante['modalidad'] == 'Tiempo parcial') {
                    $('#dat-mod-parcial').attr('checked', "checked");
                    $('#dat-mod-completo').attr('disabled', "disabled");
                } else if (data.aspirante['modalidad'] == 'Tiempo completo') {
                    $('#dat-mod-completo').attr('checked', "checked");
                    $('#dat-mod-parcial').attr('disabled', "disabled");
                } else {
                    $('#dat-mod-completo').attr('disabled', "disabled");
                    $('#dat-mod-parcial').attr('disabled', "disabled");
                }
                if (data.aspirante['estado'] == 'Aceptado')
                    $('#btn-aceptar-aspirante').addClass("hide");

                $('#dat-correo').text(
                    data.aspirante['correo']
                );
                $('.btn-recibo-de-pago').attr('id', data.aspirante['pk_matricula']);
                $('.btn-recibo-de-pago').attr('name', "_file_comprobante_pago");
                $('.btn-recibo-de-pago').attr('onclick', "popUpModalPago(this.id,this.name)");
                $('#dat-telefono').attr('value', data.aspirante['telefono']);
                $('#dat-celular').attr('value', data.aspirante['celular']);
                $('#dat-main-correo').attr('value', data.aspirante['correo']);
                $('#dat-alt-correo').attr('value', data.aspirante['correo_alt']);
                if (data.aspirante['pais'] == 'México') {
                    $('#dat-country').attr('value', data.aspirante['pais']);
                    $('#dat-entidad').attr('value', data.aspirante['entidad_federativa']);
                    $('#dat-municipio').attr('value', data.aspirante['municipio']);
                    $('#dat-localidad').attr('value', data.aspirante['delegacion']);
                    $('#dat-calle').attr('value', data.aspirante['calle']);
                    $('#dat-colonia').attr('value', data.aspirante['colonia']);
                    $('#dat-num-ext').attr('value', data.aspirante['num_ext']);
                    $('#dat-num-int').attr('value', data.aspirante['num_int']);
                } else {
                    $('#dat-country').attr('value', data.aspirante['pais']);
                    $('#country-col').removeClass("m6");
                    $('#address-col').removeClass("hide");
                    $('.ocultar').addClass("hide");
                    $('.cp').removeClass("m4");
                    $('#dat-address-full').attr('value', data.aspirante['direccion_completa']);
                }
                $('#dat-code-postal').attr('value', data.aspirante['cp']);

                $('#dat-ultimate-grado-lic').attr('value', data.aspirante['nombre_carrera_licenciatura']);
                $('#dat-institution-lic').attr('value', data.aspirante['institucion_carrera_licenciatura']);
                $('#dat-campus-lic').attr('value', data.aspirante['campus_carrera_licenciatura']);
                if (data.aspirante['pais_carrera_licenciatura'] != 'México') {
                    $('.ocultar-lic').addClass("hide");
                } else {
                    $('#dat-estado-lic').attr('value', data.aspirante['estado_institucion_licenciatura']);
                }
                $('#dat-country-grado-lic').attr('value', data.aspirante['pais_carrera_licenciatura']);
                $('#dat-promedio-lic').attr('value', data.aspirante['promedio_gral_licenciatura']);

                //CONDICIÓN ESPECIAL
                if (data.aspirante['nombre_carrera_especialidad'] != '') {
                    $('#tab-esp').removeClass("hide");
                    $('#dat-ultimate-grado-esp').attr('value', data.aspirante['nombre_carrera_especialidad']);
                    $('#dat-institution-esp').attr('value', data.aspirante['institucion_carrera_especialidad']);
                    $('#dat-campus-esp').attr('value', data.aspirante['campus_carrera_especialidad']);
                    $('#dat-country-grado-esp').attr('value', data.aspirante['pais_carrera_especialidad']);
                    $('#dat-promedio-esp').attr('value', data.aspirante['promedio_gral_especialidad']);
                    if (data.aspirante['pais_carrera_especialidad'] != 'México') {
                        $('.ocultar-esp').addClass("hide");
                    } else {
                        $('#dat-estado-esp').attr('value', data.aspirante['estado_institucion_especialidad']);
                    }
                }

                if (data.aspirante['nombre_carrera_maestria'] != '') {
                    //TAB DE MAESTRÍA
                    $('#tab-master').removeClass("hide");
                    $('#dat-ultimate-grado-master').attr('value', data.aspirante['nombre_carrera_maestria']);
                    $('#dat-institution-master').attr('value', data.aspirante['institucion_carrera_maestria']);
                    $('#dat-campus-master').attr('value', data.aspirante['campus_carrera_maestria']);
                    $('#dat-country-grado-master').attr('value', data.aspirante['pais_carrera_maestria']);
                    $('#dat-promedio-master').attr('value', data.aspirante['promedio_gral_maestria']);
                    if (data.aspirante['pais_carrera_maestria'] != 'México') {
                        $('.ocultar-master').addClass("hide");
                    } else {
                        $('#dat-estado-master').attr('value', data.aspirante['estado_institucion_maestria']);
                    }
                }

                //CONDICIÓN ESPECIAL 
                if (data.programa == 'Doctorado en Ciencias Biológicas') {

                    if (data.aspirante['pais'] != 'México' && data.aspirante['pais'] != '' && data.aspirante['pais'] != null) {
                        $('#collapser-extranjeros').removeClass("hide");
                        //APOSTILLADO DE MAESTRÍA
                        $('.card-apostillado-de-maestría').removeClass("hide");
                        if (data.aspirante_documentos['file_apostillado_certificado_maestria'] == 1) {

                            $('.btn-apostillado-de-maestría').attr("id", data.aspirante['pk_matricula']);
                            $('.btn-apostillado-de-maestría').attr("name", "_file_apostillado_certificado_maestria");
                            $('.btn-apostillado-de-maestría').attr("onclick", "popUpModal(this.id,this.name)");
                            $('.icon-apostillado-de-maestría').text("visibility");
                        } else {
                            $('.btn-apostillado-de-maestría').attr("onclick", "popUpNotUpload()");
                            $('.btn-apostillado-de-maestría').removeClass("green");
                            $('.btn-apostillado-de-maestría').addClass("red");
                            $('.icon-apostillado-de-maestría').text("visibility_off");
                        }
                    }


                    //RESUMEN DE  ANTEPROYECTO
                    $('.card-resumen-de-anteproyecto').removeClass("hide");
                    if (data.aspirante_documentos['file_resumen_anteproyecto'] == 1) {
                        $('.btn-resumen-de-anteproyecto').attr("id", data.aspirante['pk_matricula']);
                        $('.btn-resumen-de-anteproyecto').attr("name", "_file_resumen_anteproyecto");
                        $('.btn-resumen-de-anteproyecto').attr("onclick", "popUpModal(this.id,this.name)");
                        $('.icon-resumen-de-anteproyecto').text("visibility");
                    } else {
                        $('.btn-resumen-de-anteproyecto').attr("onclick", "popUpNotUpload()");
                        $('.btn-resumen-de-anteproyecto').removeClass("green");
                        $('.btn-resumen-de-anteproyecto').addClass("red");
                        $('.icon-resumen-de-anteproyecto').text("visibility_off");
                    }

                    //CARTA DE ACEPTACION
                    $('.card-carta-de-aceptación').removeClass("hide");
                    if (data.aspirante_documentos['file_carta_aceptacion'] == 1) {
                        $('.btn-carta-de-aceptación').attr("id", data.aspirante['pk_matricula']);
                        $('.btn-carta-de-aceptación').attr("name", "_file_carta_aceptacion");
                        $('.btn-carta-de-aceptación').attr("onclick", "popUpModal(this.id,this.name)");
                        $('.icon-carta-de-aceptación').text("visibility");
                    } else {
                        $('.btn-carta-de-aceptación').attr("onclick", "popUpNotUpload()");
                        $('.btn-carta-de-aceptación').removeClass("green");
                        $('.btn-carta-de-aceptación').addClass("red");
                        $('.icon-carta-de-aceptación').text("visibility_off");
                    }

                    //CERTIFICADO DE CALIFICACIONES MAESTRÍA
                    $('#tab-master-documents').removeClass("hide");
                    if (data.aspirante_documentos['file_certificado_calificaciones_maestria'] == 1) {
                        $('.btn-certificado-de-calificaciones-maestría').attr("id", data.aspirante['pk_matricula']);
                        $('.btn-certificado-de-calificaciones-maestría').attr("name", "_file_certificado_calificaciones_maestria");
                        $('.btn-certificado-de-calificaciones-maestría').attr("onclick", "_file_certificado_calificaciones_maestria");
                        $('.icon-certificado-de-calificaciones-maestría').text("visibility");
                    } else {
                        $('.btn-certificado-de-calificaciones-maestría').attr("onclick", "popUpNotUpload()");
                        $('.btn-certificado-de-calificaciones-maestría').removeClass("green");
                        $('.btn-certificado-de-calificaciones-maestría').addClass("red");
                        $('.icon-certificado-de-calificaciones-maestría').text("visibility_off");
                    }

                    //TITULO DE MAESTRÍA
                    if (data.aspirante_documentos['file_titulo_maestria'] == 1) {
                        $('.btn-titulo-de-maestría').attr("id", data.aspirante['pk_matricula']);
                        $('.btn-titulo-de-maestría').attr("name", "_file_titulo_maestria");
                        $('.btn-titulo-de-maestría').attr("onclick", "popUpModal(this.id,this.name)");
                        $('.icon-titulo-de-maestría').text("visibility");
                    } else {
                        $('.btn-titulo-de-maestría').attr("onclick", "popUpNotUpload()");
                        $('.btn-titulo-de-maestría').removeClass("green");
                        $('.btn-titulo-de-maestría').addClass("red");
                        $('.icon-titulo-de-maestría').text("visibility_off");
                    }

                    //CEDULA DE MAESTRÍA
                    if (data.aspirante_documentos['file_cedula_maestria'] == 1) {
                        $('.btn-cedula-de-maestría').attr("id", data.aspirante['pk_matricula']);
                        $('.btn-cedula-de-maestría').attr("name", "_file_cedula_maestria");
                        $('.btn-cedula-de-maestría').attr("onclick", "popUpModal(this.id,this.name)");
                        $('.icon-cedula-de-maestría').text("visibility");
                    } else {
                        $('.btn-cedula-de-maestría').attr("onclick", "popUpNotUpload()");
                        $('.btn-cedula-de-maestría').removeClass("green");
                        $('.btn-cedula-de-maestría').addClass("red");
                        $('.icon-cedula-de-maestría').text("visibility_off");
                    }

                }

                //CONDICIÓN ESPECIAL 
                if (data.programa == 'Maestría en Ciencias Biológicas') {

                    //RESUMEN DE ANTEPROYECTO
                    $('.card-resumen-de-anteproyecto').removeClass("hide");
                    if (data.aspirante_documentos['file_resumen_anteproyecto'] == 1) {
                        $('.btn-resumen-de-anteproyecto').attr("id", data.aspirante['pk_matricula']);
                        $('.btn-resumen-de-anteproyecto').attr("name", "_file_resumen_anteproyecto");
                        $('.btn-resumen-de-anteproyecto').attr("onclick", "popUpModal(this.id,this.name)");
                        $('.icon-resumen-de-anteproyecto').text("visibility");
                    } else {
                        $('.btn-resumen-de-anteproyecto').attr("onclick", "popUpNotUpload()");
                        $('.btn-resumen-de-anteproyecto').removeClass("green");
                        $('.btn-resumen-de-anteproyecto').addClass("red");
                        $('.icon-resumen-de-anteproyecto').text("visibility_off");
                    }

                    //CARTA DE ACEPTACIÓN
                    $('.card-carta-de-aceptación').removeClass("hide");
                    if (data.aspirante_documentos['file_carta_aceptacion'] == 1) {
                        $('.btn-carta-de-aceptación').attr("id", data.aspirante['pk_matricula']);
                        $('.btn-carta-de-aceptación').attr("name", "_file_carta_aceptacion");
                        $('.btn-carta-de-aceptación').attr("onclick", "popUpModal(this.id,this.name)");
                        $('.icon-carta-de-aceptación').text("visibility");
                    } else {
                        $('.btn-carta-de-aceptación').attr("onclick", "popUpNotUpload()");
                        $('.btn-carta-de-aceptación').removeClass("green");
                        $('.btn-carta-de-aceptación').addClass("red");
                        $('.icon-carta-de-aceptación').text("visibility_off");
                    }

                }

                //CONDICIÓN ESPECIAL 
                if (data.programa == 'Maestría en Salud y Producción Animal Sustentable') {

                    //CARTA DE JEFE INMEDIATO
                    $('.card-carta-de-jefe-inmediato').removeClass("hide");
                    if (data.aspirante_documentos['file_carta_jefe'] == 1) {
                        $('.btn-carta-de-jefe-inmediato').attr("id", data.aspirante['pk_matricula']);
                        $('.btn-carta-de-jefe-inmediato').attr("name", "_file_carta_jefe");
                        $('.btn-carta-de-jefe-inmediato').attr("onclick", "popUpModal(this.id,this.name)");
                        $('.icon-carta-de-jefe-inmediato').text("visibility");
                    } else {
                        $('.btn-carta-de-jefe-inmediato').attr("onclick", "popUpNotUpload()");
                        $('.btn-carta-de-jefe-inmediato').removeClass("green");
                        $('.btn-carta-de-jefe-inmediato').addClass("red");
                        $('.icon-carta-de-jefe-inmediato').text("visibility_off");
                    }

                }

                if (data.aspirante['pais'] != 'México' && data.aspirante['pais'] != '' && data.aspirante['pais'] != null) {
                    $('#collapser-extranjeros').removeClass("hide");
                    //COMPROBANTE DE IDENTIDAD
                    if (data.aspirante_documentos['file_apostillado_certificado_licenciatura'] == 1) {
                        $('.btn-apostillado-de-licenciatura').attr("id", data.aspirante['pk_matricula']);
                        $('.btn-apostillado-de-licenciatura').attr("name", "_file_apostillado_certificado_licenciatura");
                        $('.btn-apostillado-de-licenciatura').attr("onclick", "popUpModal(this.id,this.name)");
                        $('.icon-apostillado-de-licenciatura').text("visibility");
                    } else {
                        $('.btn-apostillado-de-licenciatura').attr("onclick", "popUpNotUpload()");
                        $('.btn-apostillado-de-licenciatura').removeClass("green");
                        $('.btn-apostillado-de-licenciatura').addClass("red");
                        $('.icon-apostillado-de-licenciatura').text("visibility_off");
                    }
                }

                //SE COMIENZAN A CARGAR LOS BOTONES CON LA FUNCIÓN CORRESPONDIENTE

                //ACTA DE NACIMIENTO
                if (data.aspirante_documentos['file_acta_nacimiento'] == 1) {
                    $('.btn-acta-de-nacimiento').attr("id", data.aspirante['pk_matricula']);
                    $('.btn-acta-de-nacimiento').attr("name", "_file_acta_nacimiento");
                    $('.btn-acta-de-nacimiento').attr("onclick", "popUpModal(this.id,this.name)");
                    $('.icon-acta-de-nacimiento').text("visibility");
                } else {
                    $('.btn-acta-de-nacimiento').attr("onclick", "popUpNotUpload()");
                    $('.btn-acta-de-nacimiento').removeClass("green");
                    $('.btn-acta-de-nacimiento').addClass("red");
                    $('.icon-acta-de-nacimiento').text("visibility_off");
                }

                //CERTIFICADO DE CALIFICACIONES LICENCIATURA
                if (data.aspirante_documentos['file_certificado_calificaciones_licenciatura'] == 1) {
                    $('.btn-certificado-de-calificaciones-licenciatura').attr("id", data.aspirante['pk_matricula']);
                    $('.btn-certificado-de-calificaciones-licenciatura').attr("name", "_file_certificado_calificaciones_licenciatura");
                    $('.btn-certificado-de-calificaciones-licenciatura').attr("onclick", "popUpModal(this.id,this.name)");

                    $('.icon-certificado-de-calificaciones-licenciatura').text("visibility");
                } else {
                    $('.btn-certificado-de-calificaciones-licenciatura').attr("onclick", "popUpNotUpload()");
                    $('.btn-certificado-de-calificaciones-licenciatura').removeClass("green");
                    $('.btn-certificado-de-calificaciones-licenciatura').addClass("red");
                    $('.icon-certificado-de-calificaciones-licenciatura').text("visibility_off");
                }

                //COMPROBANTE DE DOMICILIO
                if (data.aspirante_documentos['file_comprobante_domicilio'] == 1) {
                    $('.btn-comprobante-de-domicilio').attr("id", data.aspirante['pk_matricula']);
                    $('.btn-comprobante-de-domicilio').attr("name", "_file_comprobante_domicilio");
                    $('.btn-comprobante-de-domicilio').attr("onclick", "popUpModal(this.id,this.name)");
                    $('.icon-comprobante-de-domicilio').text("visibility");
                } else {
                    $('.btn-comprobante-de-domicilio').attr("onclick", "popUpNotUpload()");
                    $('.btn-comprobante-de-domicilio').removeClass("green");
                    $('.btn-comprobante-de-domicilio').addClass("red");
                    $('.icon-comprobante-de-domicilio').text("visibility_off");
                }

                //TITULO DE LICENCIATURA
                if (data.aspirante_documentos['file_titulo_licenciatura'] == 1) {
                    $('.btn-titulo-de-licenciatura').attr("id", data.aspirante['pk_matricula']);
                    $('.btn-titulo-de-licenciatura').attr("name", "_file_titulo_licenciatura");
                    $('.btn-titulo-de-licenciatura').attr("onclick", "popUpModal(this.id,this.name)");
                    $('.icon-titulo-de-licenciatura').text("visibility");
                } else {
                    $('.btn-titulo-de-licenciatura').attr("onclick", "popUpNotUpload()");
                    $('.btn-titulo-de-licenciatura').removeClass("green");
                    $('.btn-titulo-de-licenciatura').addClass("red");
                    $('.icon-titulo-de-licenciatura').text("visibility_off");
                }

                //CEDULA DE LICENCIATURA
                if (data.aspirante_documentos['file_cedula_licenciatura'] == 1) {
                    $('.btn-cedula-de-licenciatura').attr("id", data.aspirante['pk_matricula']);
                    $('.btn-cedula-de-licenciatura').attr("name", "_file_cedula_licenciatura");
                    $('.btn-cedula-de-licenciatura').attr("onclick", "popUpModal(this.id,this.name)");
                    $('.icon-cedula-de-licenciatura').text("visibility");
                } else {
                    $('.btn-cedula-de-licenciatura').attr("onclick", "popUpNotUpload()");
                    $('.btn-cedula-de-licenciatura').removeClass("green");
                    $('.btn-cedula-de-licenciatura').addClass("red");
                    $('.icon-cedula-de-licenciatura').text("visibility_off");
                }

                //FOTOGRAFÍA
                if (data.aspirante_documentos['file_fotografia'] == 1) {
                    $('.btn-fotografía').attr("id", data.aspirante['pk_matricula']);
                    $('.btn-fotografía').attr("name", "_file_fotografia");
                    $('.btn-fotografía').attr("onclick", "popUpModal(this.id,this.name)");
                    $('.icon-fotografía').text("visibility");
                } else {
                    $('.btn-fotografía').attr("onclick", "popUpNotUpload()");
                    $('.btn-fotografía').removeClass("green");
                    $('.btn-fotografía').addClass("red");
                    $('.icon-fotografía').text("visibility_off");
                }

                //CURRICULUM
                if (data.aspirante_documentos['file_cv'] == 1) {
                    $('.btn-cv').attr("id", data.aspirante['pk_matricula']);
                    $('.btn-cv').attr("name", "_file_cv");
                    $('.btn-cv').attr("onclick", "popUpModal(this.id,this.name)");
                    $('.icon-cv').text("visibility");
                } else {
                    $('.btn-cv').attr("onclick", "popUpNotUpload()");
                    $('.btn-cv').removeClass("green");
                    $('.btn-cv').addClass("red");
                    $('.icon-cv').text("visibility_off");
                }

                //CARTA INTENCIÓN
                if (data.aspirante_documentos['file_carta_intencion'] == 1) {
                    $('.btn-carta-de-intención').attr("id", data.aspirante['pk_matricula']);
                    $('.btn-carta-de-intención').attr("name", "_file_carta_intencion");
                    $('.btn-carta-de-intención').attr("onclick", "popUpModal(this.id,this.name)");
                    $('.icon-carta-de-intención').text("visibility");
                } else {
                    $('.btn-carta-de-intención').attr("onclick", "popUpNotUpload()");
                    $('.btn-carta-de-intención').removeClass("green");
                    $('.btn-carta-de-intención').addClass("red");
                    $('.icon-carta-de-intención').text("visibility_off");
                }

                //COMPROBANTE DE IDENTIDAD
                if (data.aspirante_documentos['file_comprobante_identidad'] == 1) {
                    $('.btn-comprobante-de-identidad').attr("id", data.aspirante['pk_matricula']);
                    $('.btn-comprobante-de-identidad').attr("name", "_file_comprobante_identidad");
                    $('.btn-comprobante-de-identidad').attr("onclick", "popUpModal(this.id,this.name)");
                    $('.icon-comprobante-de-identidad').text("visibility");
                } else {
                    $('.btn-comprobante-de-identidad').attr("onclick", "popUpNotUpload()");
                    $('.btn-comprobante-de-identidad').removeClass("green");
                    $('.btn-comprobante-de-identidad').addClass("red");
                    $('.icon-comprobante-de-identidad').text("visibility_off");
                }

                //RECOMENDACIONES
                if (data.success_recomendaciones) {
                    referencias = 1;
                    $('#nombre-carta1').text(data.aspirante_recomendaciones[0]['nombre_recomendante']);
                    $('#institución-carta1').text(data.aspirante_recomendaciones[0]['lugar_trabajo']);
                    $('#telefono-carta1').text(data.aspirante_recomendaciones[0]['telefono']);
                    $('#descripción-carta1').text(data.aspirante_recomendaciones[0]['descripcion']);
                    $('#consideraciones-carta1').text(data.aspirante_recomendaciones[0]['habilidades_debilidades']);
                    if (data.aspirante_recomendaciones[0]['conocimientos'] == "Excelente")
                        $('#c-carta1').addClass("green accent-4");
                    else if (data.aspirante_recomendaciones[0]['conocimientos'] == "Bueno")
                        $('#c-carta1').addClass("blue");
                    else if (data.aspirante_recomendaciones[0]['conocimientos'] == "Regular")
                        $('#c-carta1').addClass("amber accent-4");
                    else if (data.aspirante_recomendaciones[0]['conocimientos'] == "Deficiente")
                        $('#c-carta1').addClass("red");
                    if (data.aspirante_recomendaciones[0]['dedicacion'] == "Excelente")
                        $('#d-carta1').addClass("green accent-4");
                    else if (data.aspirante_recomendaciones[0]['dedicacion'] == "Bueno")
                        $('#d-carta1').addClass("blue");
                    else if (data.aspirante_recomendaciones[0]['dedicacion'] == "Regular")
                        $('#d-carta1').addClass("amber accent-4");
                    else if (data.aspirante_recomendaciones[0]['dedicacion'] == "Deficiente")
                        $('#d-carta1').addClass("red");
                    if (data.aspirante_recomendaciones[0]['comunicacion'] == "Excelente")
                        $('#h-carta1').addClass("green accent-4");
                    else if (data.aspirante_recomendaciones[0]['comunicacion'] == "Bueno")
                        $('#h-carta1').addClass("blue");
                    else if (data.aspirante_recomendaciones[0]['comunicacion'] == "Regular")
                        $('#h-carta1').addClass("amber accent-4");
                    else if (data.aspirante_recomendaciones[0]['comunicacion'] == "Deficiente")
                        $('#h-carta1').addClass("red");
                    if (data.aspirante_recomendaciones[0]['iniciativa'] == "Excelente")
                        $('#i-carta1').addClass("green accent-4");
                    else if (data.aspirante_recomendaciones[0]['iniciativa'] == "Bueno")
                        $('#i-carta1').addClass("blue");
                    else if (data.aspirante_recomendaciones[0]['iniciativa'] == "Regular")
                        $('#i-carta1').addClass("amber accent-4");
                    else if (data.aspirante_recomendaciones[0]['iniciativa'] == "Deficiente")
                        $('#i-carta1').addClass("red");
                    if (data.aspirante_recomendaciones[0]['perseverancia'] == "Excelente")
                        $('#p-carta1').addClass("green accent-4");
                    else if (data.aspirante_recomendaciones[0]['perseverancia'] == "Bueno")
                        $('#p-carta1').addClass("blue");
                    else if (data.aspirante_recomendaciones[0]['perseverancia'] == "Regular")
                        $('#p-carta1').addClass("amber accent-4");
                    else if (data.aspirante_recomendaciones[0]['perseverancia'] == "Deficiente")
                        $('#p-carta1').addClass("red");
                    if (data.aspirante_recomendaciones[0]['actitud'] == "Excelente")
                        $('#a-carta1').addClass("green accent-4");
                    else if (data.aspirante_recomendaciones[0]['actitud'] == "Bueno")
                        $('#a-carta1').addClass("blue");
                    else if (data.aspirante_recomendaciones[0]['actitud'] == "Regular")
                        $('#a-carta1').addClass("amber accent-4");
                    else if (data.aspirante_recomendaciones[0]['actitud'] == "Deficiente")
                        $('#a-carta1').addClass("red");
                    /////////////////////////////
                    $('#nombre-carta2').text(data.aspirante_recomendaciones[1]['nombre_recomendante']);
                    $('#institución-carta2').text(data.aspirante_recomendaciones[1]['lugar_trabajo']);
                    $('#telefono-carta2').text(data.aspirante_recomendaciones[1]['telefono']);
                    $('#descripción-carta2').text(data.aspirante_recomendaciones[1]['descripcion']);
                    $('#consideraciones-carta2').text(data.aspirante_recomendaciones[1]['habilidades_debilidades']);
                    if (data.aspirante_recomendaciones[1]['conocimientos'] == "Excelente")
                        $('#c-carta2').addClass("green accent-4");
                    else if (data.aspirante_recomendaciones[1]['conocimientos'] == "Bueno")
                        $('#c-carta2').addClass("blue");
                    else if (data.aspirante_recomendaciones[1]['conocimientos'] == "Regular")
                        $('#c-carta2').addClass("amber accent-4");
                    else if (data.aspirante_recomendaciones[1]['conocimientos'] == "Deficiente")
                        $('#c-carta2').addClass("red");
                    if (data.aspirante_recomendaciones[1]['dedicacion'] == "Excelente")
                        $('#d-carta2').addClass("green accent-4");
                    else if (data.aspirante_recomendaciones[1]['dedicacion'] == "Bueno")
                        $('#d-carta2').addClass("blue");
                    else if (data.aspirante_recomendaciones[1]['dedicacion'] == "Regular")
                        $('#d-carta2').addClass("amber accent-4");
                    else if (data.aspirante_recomendaciones[1]['dedicacion'] == "Deficiente")
                        $('#d-carta2').addClass("red");
                    if (data.aspirante_recomendaciones[1]['comunicacion'] == "Excelente")
                        $('#h-carta2').addClass("green accent-4");
                    else if (data.aspirante_recomendaciones[1]['comunicacion'] == "Bueno")
                        $('#h-carta2').addClass("blue");
                    else if (data.aspirante_recomendaciones[1]['comunicacion'] == "Regular")
                        $('#h-carta2').addClass("amber accent-4");
                    else if (data.aspirante_recomendaciones[1]['comunicacion'] == "Deficiente")
                        $('#h-carta2').addClass("red");
                    if (data.aspirante_recomendaciones[1]['iniciativa'] == "Excelente")
                        $('#i-carta2').addClass("green accent-4");
                    else if (data.aspirante_recomendaciones[1]['iniciativa'] == "Bueno")
                        $('#i-carta2').addClass("blue");
                    else if (data.aspirante_recomendaciones[1]['iniciativa'] == "Regular")
                        $('#i-carta2').addClass("amber accent-4");
                    else if (data.aspirante_recomendaciones[1]['iniciativa'] == "Deficiente")
                        $('#i-carta2').addClass("red");
                    if (data.aspirante_recomendaciones[1]['perseverancia'] == "Excelente")
                        $('#p-carta2').addClass("green accent-4");
                    else if (data.aspirante_recomendaciones[1]['perseverancia'] == "Bueno")
                        $('#p-carta2').addClass("blue");
                    else if (data.aspirante_recomendaciones[1]['perseverancia'] == "Regular")
                        $('#p-carta2').addClass("amber accent-4");
                    else if (data.aspirante_recomendaciones[1]['perseverancia'] == "Deficiente")
                        $('#p-carta2').addClass("red");
                    if (data.aspirante_recomendaciones[1]['actitud'] == "Excelente")
                        $('#a-carta2').addClass("green accent-4");
                    else if (data.aspirante_recomendaciones[1]['actitud'] == "Bueno")
                        $('#a-carta2').addClass("blue");
                    else if (data.aspirante_recomendaciones[1]['actitud'] == "Regular")
                        $('#a-carta2').addClass("amber accent-4");
                    else if (data.aspirante_recomendaciones[1]['actitud'] == "Deficiente")
                        $('#a-carta2').addClass("red");


                }
                //////////////////////////////////////
                //TAB ISSUES FIX
                /////////////////////////////////////
                $('.div-ponderación').removeClass("hide");
                $('.div-ponderación').hide("fast");
                ////////////////////////////////////////
                //PONDERACIONES            
                ////////////////////////////////////////                
                fillingPonderation();


            } else {
                // location.href = "consultas.html";
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            //location.href = "consultas.html";
            //alert("Mistake");
        }

    });
}

/*---------------------------------------
   FUNCIÓN FILL PONDERACIÓN 
----------------------------------------*/
function fillingPonderation() { //Función que llena los sliders de ponderación con los datos de cada item en la tabla de ponderaciones

    var url = "php/Consultas/consulta-aspirante.php"; // El script a dónde se realizará la petición.
    var parametros = {
        "accion": "consultar-ponderación"
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        dataType: "JSON",
        success: function (data) {
            if (data.success_ponderación) {
                $('#collapsible-percents').empty();
                var sliderStart = 0.0;

                $.each(data.items_ponderación, function (index) {

                    var $block = $("<blockquote>", {}),
                        $h6 = $("<label>", {
                            text: data.items_ponderación[index]['ponderacion']
                        }),
                        $chip = $("<div>", {
                            class: "chip",
                            id: "chip" + data.items_ponderación[index]['pk_ponderacion']
                        }),
                        $slider = $("<div>", {
                            id: "slider" + data.items_ponderación[index]['pk_ponderacion'],
                            class: "slider-style"
                        });
                    $h6.append($chip);
                    $block.append($h6);
                    $block.append($slider);
                    $('#collapsible-percents').append($block);

                    $('#chip' + data.items_ponderación[index][0]).text(data.items_ponderación[index]['valor'] + "%");

                    var snapSlider = document.getElementById('slider' + data.items_ponderación[index]['pk_ponderacion']);

                    if (data.flag_ponderación) {
                        sliderStart = data.items_ponderación[index]['valor_alcanzado'];
                    }
                    slidersValues[index] = parseInt(sliderStart);
                    noUiSlider.create(snapSlider, {
                        start: parseInt(sliderStart),
                        animate: true,
                        behaviour: 'tap',
                        connect: [true, false],
                        tooltips: [wNumb({
                            decimals: 0,
                            postfix: '%'
                        })],
                        range: {
                            'min': 0,
                            'max': parseInt(data.items_ponderación[index]['valor'])
                        },
                        format: wNumb({
                            decimals: 0,
                            thousand: '.',
                            postfix: ' %',
                        })

                    });
                    slidersName[index] = snapSlider;
                    slidersFk[index] = data.items_ponderación[index]['pk_ponderacion'];
                    if (data.periodo_activo != 'just_now') {
                        snapSlider.setAttribute('disabled', true);
                    } else if (data.periodo_activo == 'just_now') {
                        snapSlider.noUiSlider.on('slide', function (values, handle) {
                            //$('#chip' + data.items_ponderación[index][0]).text(values[handle]);
                            updatePercent();
                            $('.mybadge').addClass("yellow darken-4");
                            $('#chipPercent').addClass("yellow darken-4 chipPercent");
                        });
                    }

                });
                if (data.periodo_activo == 'just_now') {
                    var $btngavel = $("<a>", {
                            class: "waves-effect waves-light btn right",
                            onclick: "ponderar()"
                        }),
                        $igavel = $("<i>", {
                            class: "material-icons",
                            text: "gavel"
                        }),
                        $divBTN = $("<div>", {
                            class: "div-gavel"
                        });
                    $btngavel.append($igavel);
                    $btngavel.append("Guardar");
                    var $btnReset = $("<a>", {
                            class: "waves-effect waves-light btn left",
                            onclick: "resetPonderar()"
                        }),
                        $iReset = $("<i>", {
                            class: "material-icons",
                            text: "cached"
                        }),
                        $divReset = $("<div>", {
                            class: "div-gavel"
                        });
                    $btnReset.append($iReset);
                    $btnReset.append("Reset");
                    $divBTN.append($btnReset);
                    $divBTN.append($btngavel);
                    $('#collapsible-percents').append($divBTN);
                    updatePercent();
                } else {
                    updatePercent();
                }
                kuonyesha(3);
                ponderaciones = 1;
            } else {
                kuonyesha(3);

            }
        },
        error: function (jqXhr, textStatus, errorThrown) {}
    });

}

/*-------------------------------------------
   EVENTO ACTULIZAR PORCENTAJE PONDERACIÓN
-------------------------------------------*/
function updatePercent() {
    var percent = 0;
    $.each(slidersName, function (index) {
        percent += parseInt(slidersName[index].noUiSlider.get());
    });
    $('#percent').empty();
    $('#percent').append(percent + "%");
    $('#chipPercent').text(percent + "%");
}

/*-----------------------------------
   FUNCTION RESET PONDERACIÓN
-----------------------------------*/

function resetPonderar() {
    $.each(slidersName, function (index) {
        slidersName[index].noUiSlider.set(slidersValues[index]);
    });
    updatePercent();
    $('.mybadge').removeClass("yellow darken-4");
    $('#chipPercent').removeClass("yellow darken-4 chipPercent");
}
/*-----------------------------------
   FUNCIÓN ACTUALIZAR PONDERACIÓN
-----------------------------------*/
function ponderar() {
    kuonyesha(2);
    var itemReachValue = [];
    $.each(slidersName, function (index) {
        itemReachValue[index] = slidersName[index].noUiSlider.get();
    });
    var url = "php/Consultas/consulta-aspirante.php"; // El script a dónde se realizará la petición.
    var parametros = {
        "accion": "ponderar",
        "itemPk": slidersFk,
        "itemReachValue": itemReachValue
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        dataType: "JSON",
        success: function (data) {
            if (data.success) {
                Materialize.toast('¡Exito!, Ponderación guardada.', 5000);
                $('.mybadge').removeClass("yellow darken-4");
                $('#chipPercent').removeClass("yellow darken-4 chipPercent");
                fillingPonderation();
            } else {
                Materialize.toast('¡Error!, Algo salió mal.', 5000);
                kuonyesha(2);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            Materialize.toast('¡Falló!, Vuelva a intentar.', 5000);
            kuonyesha(2);
        }

    });
}

/*-----------------------------------
   EVENTO TOAST 
-----------------------------------*/
function popUpNotUpload() {
    Materialize.toast('¡El aspirante aún no a proporcionado este documento!', 5000);
}
/*-----------------------------------
   EVENTO MODAL PDF
-----------------------------------*/
/*Evento que llenara el modal con el archivo pdf correspondiente del aspirante*/
function popUpModal(pk_aspirante, documentName) {
    $("#pdf-object").attr("data", "cuestionarios/PDF/" + pk_aspirante + documentName);
    $("#pdf-link").attr("href", "cuestionarios/PDF/" + pk_aspirante + documentName);
    $('#modal-pdf').modal('open');
}

/*-----------------------------------
   EVENTO MODAL PDF
-----------------------------------*/
/*Evento que llenara el modal con el archivo pdf correspondiente del aspirante*/
function popUpModalPago(pk_aspirante, documentName) {
    $('.overlay').removeClass("hide");
    kuonyesha(4);
    var url = "php/Consultas/consultas.php"; // El script a dónde se realizará la petición.
    var parametros = {
        "accion": "documento_Exist",
        "documento": documentName,
        "pk_aspirante": pk_aspirante //Pk del alumno
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        dataType: "JSON",
        success: function (data) {
            if (data.exist) {

                $("#pdf-object-pago").attr("data", "cuestionarios/PDF/" + pk_aspirante + documentName);
                $("#pdf-link-pago").attr("href", "cuestionarios/PDF/" + pk_aspirante + documentName);
                $("#input-banco").attr("value", data.banco[0]['pago_banco']);
                $("#input-folio").attr("value", data.banco[0]['pago_folio']);
                $("#input-fecha").attr("value", data.banco[0]['pago_fecha']);
                $("#input-referencia").attr("value", data.banco[0]['pago_referencia1']);
                $("#input-expediente").attr("value", data.banco[0]['pago_expediente']);

                kuonyesha(5);
                $('#modal-pdf-pago').modal('open');

            } else {
                kuonyesha(5);
                Materialize.toast('¡El aspirante aún no a proporcionado este documento!', 5000);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error 
            kuonyesha(5);
            Materialize.toast('¡El servidor no responde!', 5000);
        }
    });
}


/*---------------------------------------
    FUNCIÓN PARA ACEPTAR ASPIRANTE
---------------------------------------*/
function acceptAspirant() {
    kuonyesha(2);
    var url = "php/Consultas/consulta-aspirante.php"; // El script a dónde se realizará la petición.
    var parametros = {
        "accion": "aceptar-aspirante"
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        dataType: "JSON",
        success: function (data) {
            if (data.success) {
                $('#modal-aceptar').modal('close');
                $('#btn-aceptar-aspirante').addClass("hide");
                Materialize.toast('¡Exito!, El aspirante fue aceptado.', 5000);
                kuonyesha(3);

            } else {
                kuonyesha(3);
                Materialize.toast('¡Falló!, Vuelva a intentarlo.', 5000);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error 
            kuonyesha(3);
            Materialize.toast('¡Error!, El servidor no responde.', 5000);
        }
    });
}
/*----------------------------------
    MENÚ DE NAVBAR
----------------------------------*/
function loadPage() { //Carga la página de aspirante
    window.close();
}


/*---------------------------------
    OCULTAR Y MOSTRAR BOTON IR ARRIBA
 ----------------------------------*/
$(function () {
    $(window).scroll(function () {
        var scrolltop = $(this).scrollTop();
        if (scrolltop >= 50) {
            $(".ir-arriba").fadeIn();
        } else {
            $(".ir-arriba").fadeOut();
        }
    });

});

/*--------------------------------------
    EVITAR ONCLICK POR DEFAULT EN LINK
 -------------------------------------*/
$(document).on("click", "a", function (e) {
    e.preventDefault();
})


/*--------------------------------
EVENTOS TAB 
----------------------------------*/


$(".click-datos").click(function () {
    $('.div-datos').hide(10);
    $('.div-documentos').hide("slow");
    $('.div-cartas').hide("slow");
    $('.div-ponderación').hide("slow");
    $('.div-datos').show("slow");
    $('.li-datos').addClass("k", 1000, "easeInBack");
    $('.li-documentos').removeClass("k", 1000, "easeInBack");
    $('.li-cartas').removeClass("k", 1000, "easeInBack");
    $('.li-ponderación').removeClass("k", 1000, "easeInBack");
});

$(".click-documentos").click(function () {
    $('.div-documentos').hide(10);
    $('.div-documentos').removeClass("hide");
    $('.div-datos').hide("slow");
    $('.div-cartas').hide("slow");
    $('.div-ponderación').hide("slow");
    $('.div-documentos').show("slow");
    $('.li-datos').removeClass("k", 1000, "easeInBack");
    $('.li-cartas').removeClass("k", 1000, "easeInBack");
    $('.li-ponderación').removeClass("k", 1000, "easeInBack");
    $('.li-documentos').addClass("k", 1000, "easeInBack");
});

$(".click-cartas").click(function () {
    if (referencias == 0) {
        Materialize.toast('¡Aún no hay refencias!', 5000);
    } else {
        $('.div-cartas').hide(10);
        $('.div-cartas').removeClass("hide");
        $('.div-datos').hide("slow");
        $('.div-documentos').hide("slow");
        $('.div-ponderación').hide("slow");
        $('.div-cartas').show("slow");
        $('.li-cartas').addClass("k", 1000, "easeInBack");
        $('.li-datos').removeClass("k", 1000, "easeInBack");
        $('.li-documentos').removeClass("k", 1000, "easeInBack");
        $('.li-ponderación').removeClass("k", 1000, "easeInBack");
    }
});
$(".click-ponderación").click(function () {
    if (ponderaciones == 0) {
        Materialize.toast('¡El programa aún no tiene items a ponderar asignados!', 5000);
    } else {

        $('.div-cartas').hide(10);
        $('.div-datos').hide("slow");
        $('.div-documentos').hide("slow");
        $('.div-cartas').hide("slow");
        $('.div-ponderación').show("slow");
        $('.li-cartas').removeClass("k", 1000, "easeInBack");
        $('.li-datos').removeClass("k", 1000, "easeInBack");
        $('.li-documentos').removeClass("k", 1000, "easeInBack");
        $('.li-ponderación').addClass("k", 1000, "easeInBack");
    }

});


/*-----------------------------------
   INICIALIZAR COMPONENTES
-----------------------------------*/
function inizializzare() {


    /* inizializzare == inicializar en Italiano, función que  inicializa ciertos  componentes de algunos frameworks como
       MATERIALIZE Y WOW ANIMATION*/
    /*----------------------------------
    Iniciamos smoothScroll (Scroll Suave)
    --------------------------------*/
    smoothScroll.init({
        speed: 1000, // Integer. How fast to complete the scroll in milliseconds
        offset: 0, // Integer. How far to offset the scrolling anchor location in pixels

    });

    /*----------------------
    MODALES INIT
    ------------------------*/

    $(document).ready(function () {
        $('#modal-pdf').modal();
    });

    $(document).ready(function () {
        $('#modal-pdf-pago').modal();
    });
    $(document).ready(function () {
        $('#modal-aceptar').modal();
    });
    /*----------------------
       TABS INIT
    ------------------------*/
    $(document).ready(function () {
        $('ul.tabs').tabs();
    });



    /*------------------------------------
    WOW ANIMATION
    -----------------------------------*/
    wow = new WOW({
        boxClass: 'wow', // default
        animateClass: 'animated', // default
        offset: 0, // default
        mobile: true, // default
        live: true // default
    })

    new WOW().init();

    /*--------------------------
     INIT COLLAPSIBLE
    ----------------------------*/
    $(document).ready(function () {
        $('.collapsible').collapsible({
            onOpen: function (el) {
                // https://github.com/Dogfalo/materialize/issues/2102
                window.dispatchEvent(new Event('resize'));
            }
        });
    });

    /*-----------------------------------
    PUSH SPIN
    -----------------------------------*/
    $(document).ready(function () {
        if (screen.width < 541) {
            $('.pin-top').pushpin({
                top: 813,
                bottom: 1000,
                offset: 0
            });
        } else if (screen.width < 1080) {
            $('.pin-top').pushpin({
                top: 498,
                bottom: 1000,
                offset: 0
            });
        } else {
            $('.pin-top').pushpin({
                top: 501,
                bottom: 1000,
                offset: 0
            });
        }


    });

    /*-----------------------------------
     INIT DROPDOWN
    -----------------------------------*/

    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: true, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: false, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
    });



}

/*----------------------------------
      MOSTRAR U OCULTAR CONTENIDO
----------------------------------*/
function kuonyesha(almostflag) { /* kuonyesha == mostrar en Suajili, función que oculta o muestra el div de carga que tiene un gif súper cool,también activa o des activa el scroll*/

    if (almostflag == 2) {
        $(document).ready(function () {
            $('.wait').fadeIn('fast');
            $('body').css({
                'overflow': 'hidden'
            });
        });
    } else if (almostflag == 3) {
        $(document).ready(function () {
            $('.wait').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    } else if (almostflag == 4) {
        $(document).ready(function () {
            $('.overlay').fadeIn('fast');
            $('body').css({
                'overflow': 'hidden'
            });
        });
    } else if (almostflag == 5) {
        $(document).ready(function () {
            $('.overlay').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    }
}
