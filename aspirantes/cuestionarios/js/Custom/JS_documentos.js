/*****************************************************
DOCUMENTO LISTO
******************************************************/
$(document).ready(function () {
    validar(); //Valida los documentos que ya han sido guardados y que la sesion este iniciada
    consultarPrograma(); //consulta los datos del usuario logueado
    
});

/*****************************************************
VARIABLES GLOBALES
******************************************************/
var nombre_programa = "";
var extranjero = true;
var trabaja="";

/*****************************************************
CONFIGURACIÓN DE REGLAS DE INTERFAZ Y DATOS DE 
CADA UNO DE LOS INPUT FILE DE DOCUMENTOS
******************************************************/
$('#file_comprobante_pago').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_comprobante_pago"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_apostillado_certificado_licenciatura').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_apostillado_certificado_licenciatura"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_apostillado_certificado_maestria').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_apostillado_certificado_maestria"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_fotografia').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_fotografia"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_acta_nacimiento').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_acta_nacimiento"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_comprobante_domicilio').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_comprobante_domicilio"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_comprobante_identidad').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_comprobante_identidad"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_cv').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_cv"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_carta_intencion').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_carta_intencion"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_carta_jefe').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_carta_jefe"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_certificado_calificaciones_licenciatura').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_certificado_calificaciones_licenciatura"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_certificado_calificaciones_maestria').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_certificado_calificaciones_maestria"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_titulo_licenciatura').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_titulo_licenciatura"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_cedula_licenciatura').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_cedula_licenciatura"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_carta_aceptacion').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_carta_aceptacion"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_titulo_maestria').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_titulo_maestria"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_cedula_maestria').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_cedula_maestria"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

$('#file_resumen_anteproyecto').fileinput({
    maxFileCount: 1,
    showUploadedThumbs: false,
    browseOnZoneClick: true,
    language: 'es',
    allowedFileExtensions: ['pdf'],
    uploadExtraData: {
        id: "file_resumen_anteproyecto"
    },
    uploadUrl: 'php/documentos.php?funcion=Subir'
});

/*****************************************************
EVENTO AL SUBIRSE UN DOCUMENTO
CAMBIA DE COLOR EL ICONO DEL DOCUMENTO CORRESPONDIENTE
******************************************************/
$('input.files').on('fileuploaded', function (event, data, previewId, index) {
    var idInput = $(this).attr('id');

    var respuesta = data.response[0];
    var id = data.response[1];

    if (respuesta == "Archivo subido" && id != "") {
        var idComponente = "#icono_" + id;
        $(idComponente).addClass("text-success");
        verificarArchivosFaltantes();
    }
});

/*****************************************************
EVENTO ANTES DE SUBIR UN DOCUMENTO
VALIDA QUE EL TAMAÑO DEL ARCHIVO NO EXCEDA
EL LIMITE DEFINIDO
******************************************************/
$('input.files').on('filepreupload', function (event, data, previewId, index, jqXHR) {
    /*****************************************************
    SI EL DOCUMENTO ES EL COMPROBANTE DE PAGO
    VALIDA EL FORMULARIO CORRESPONDIENTE
    ******************************************************/
    var id = data.extra.id;
    if (id == "file_comprobante_pago") {
        var $validator = $("#formularioBanco").validate();

        var $valid = $("#formularioBanco").valid();

        if ($valid) {
            registrarPago();
        } else {
            $validator.focusInvalid();
            return {
                message: 'Primero debe contestar el formulario de la derecha.',
                data: {
                    key1: 'Key 1',
                    detail1: 'Detail 1'
                }
            }
        }
    }

    ///Comprobar Tamaño del PDF
    console.log("esto es el extra");
    console.log(data.extra);
    // do your validation and return an error like below
    var customValidationFailed = true;
    var documentos = data.files[0];
    var tamaño = documentos.size;

    var limTamaño = 2;
    var limite = limTamaño * 1024 * 1024;

    if (tamaño > limite) {
        console.log("limite en mb: " + limTamaño);
        console.log("limite en bytes: " + limite);
        console.log("tamaño del documentos: " + tamaño);

        return {
            message: 'Su documento PDF no debe exceder los ' + limTamaño + ' MB.',
            data: {
                key1: 'Key 1',
                detail1: 'Detail 1'
            }
        };
    }
});

/*****************************************************
VERIFICA QUE ARCHIVOS YA HAN SIDO GUARDADOS Y REALIZA
EL CAMBIOE EN LA INTERFAZ
******************************************************/
function validar() {
    //validar archivos subidos
    var url = "./php/documentos.php?funcion=validarArchivos"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            //Si la sesion no esta iniciada
            if (data != "salir") {
                var archivos = JSON.parse(data);
                console.log(archivos);

                //validador para cada archivo
                if (archivos[0].file_comprobante_pago == 1) {
                    $("#icono_file_comprobante_pago").addClass("text-success");
                }

                if (archivos[0].file_acta_nacimiento == 1) {
                    $("#icono_file_acta_nacimiento").addClass("text-success");
                }

                if (archivos[0].file_apostillado_certificado_licenciatura == 1) {
                    $("#icono_file_apostillado_certificado_licenciatura").addClass("text-success");
                }

                if (archivos[0].file_apostillado_certificado_maestria == 1) {
                    $("#icono_file_apostillado_certificado_maestria").addClass("text-success");
                }

                if (archivos[0].file_fotografia == 1) {
                    $("#icono_file_fotografia").addClass("text-success");
                }

                if (archivos[0].file_comprobante_domicilio == 1) {
                    $("#icono_file_comprobante_domicilio").addClass("text-success");
                }

                if (archivos[0].file_comprobante_identidad == 1) {
                    $("#icono_file_comprobante_identidad").addClass("text-success");
                }

                if (archivos[0].file_cv == 1) {
                    $("#icono_file_cv").addClass("text-success");
                }

                if (archivos[0].file_carta_intencion == 1) {
                    $("#icono_file_carta_intencion").addClass("text-success");
                }

                if (archivos[0].file_carta_jefe == 1) {
                    $("#icono_file_carta_jefe").addClass("text-success");
                }

                if (archivos[0].file_certificado_calificaciones_licenciatura == 1) {
                    $("#icono_file_certificado_calificaciones_licenciatura").addClass("text-success");
                }

                if (archivos[0].file_titulo_licenciatura == 1) {
                    $("#icono_file_titulo_licenciatura").addClass("text-success");
                }

                if (archivos[0].file_cedula_licenciatura == 1) {
                    $("#icono_file_cedula_licenciatura").addClass("text-success");
                }

                if (archivos[0].file_carta_aceptacion == 1) {
                    $("#icono_file_carta_aceptacion").addClass("text-success");
                }

                if (archivos[0].file_titulo_maestria == 1) {
                    $("#icono_file_titulo_maestria").addClass("text-success");
                }

                if (archivos[0].file_cedula_maestria == 1) {
                    $("#icono_file_cedula_maestria").addClass("text-success");
                }
                
                if (archivos[0].file_resumen_anteproyecto == 1) {
                    $("#icono_file_resumen_anteproyecto").addClass("text-success");
                }
                
                if (archivos[0].file_certificado_calificaciones_maestria == 1) {
                    $("#icono_file_certificado_calificaciones_maestria").addClass("text-success");
                }

                //Mostrar Pagina
                document.getElementById('cargando').style.display = 'none';
                $('html, body').css({
                    'overflow': 'auto'
                });

            } else location.href = "../login.php";
        }
    });

    //consultar campos de pago
    var url = "./php/documentos.php?funcion=consultarDatosPago"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            //Si la sesion no esta iniciada
            if (data != "salir") {
                var datos = JSON.parse(data);
                $("#inputBanco").val(datos[0].pago_banco);
                $("#inputFecha").val(datos[0].pago_fecha);
                $("#inputFolio").val(datos[0].pago_folio);
                $("#inputExpediente").val(datos[0].pago_expediente);
                $("#inputReferencia").val(datos[0].pago_referencia1);

            } else location.href = "../login.php";
        }
    });
}

/*****************************************************
VERIFICA QUE DOCUMENTOS FALTAN DEPENDIENDO DE LOS
CASOS ESPECIALES DEL PROGRAMA AL QUE SE POSTULA
******************************************************/
function verificarArchivosFaltantes() {
    var url = "./php/documentos.php?funcion=validarArchivos"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            //si la sesion no se ha iniciado
            if (data != "salir") {
                var archivos = JSON.parse(data);
                console.log("Programa: "+nombre_programa);
                console.log(archivos);
                //Banderas de decision
                var masUno = false;
                var todos = true;
                var ninguno = false;

                //verificacion de archivos y definicion de banderas
                var cantidadArchivos=19;
                for (var i = 1; i < cantidadArchivos; i++) {
                    var v = archivos[0][i];
                    console.log("Evaluando: "+v+" en i="+i);
                    //si almenos un archivo se ha subido
                    if (v == 1) {
                        masUno = true;
                        console.log("masUno = true");
                    }
                    //si falta un archivo
                    if (v != 1) {
                        //casos especiales
                        var omitido = false;

                        //titulo de maestria
                        if (nombre_programa != "Doctorado en Ciencias Biológicas" && i == 6) {
                            console.log("Titulo de maestria omitido");
                            omitido = true;
                        }
                        
                        //certificado de maestria
                        if (nombre_programa != "Doctorado en Ciencias Biológicas" && i == 18) {
                            console.log("Certificado de maestria omitido");
                            omitido = true;
                        }

                        //cedula de maestria
                        if (nombre_programa != "Doctorado en Ciencias Biológicas" && i == 7) {
                            console.log("Cedula de maestria omitido");
                            omitido = true;

                        }

                        //carta de jefe inmediato
                        if (nombre_programa != "Maestría en Salud y Producción Animal Sustentable" && i == 13) {
                            console.log("Carta de jefe inmediato omitida");
                            omitido = true;

                        }else if(trabaja=="No" && i==13){
                             console.log("Carta de jefe inmediato omitida");
                            omitido = true;
                        }

                        //carta de aceptacion
                        if (nombre_programa != "Doctorado en Ciencias Biológicas" && i == 14 && nombre_programa != "Maestría en Ciencias Biológicas" && i == 14) {
                            console.log("Carta de aceptacion omitida");
                            omitido = true;

                        }
                        
                        //resumen de anteproyecto
                        if (nombre_programa != "Doctorado en Ciencias Biológicas" && i == 17 && nombre_programa != "Maestría en Ciencias Biológicas" && i == 17) {
                            console.log("Resumen de anteproyecto omitido");
                            omitido = true;

                        }
                        
                        //Apostillado de Maestria
                        if (extranjero == true && i == 16 && nombre_programa != "Doctorado en Ciencias Biológicas") {
                            console.log("Apostillado de Maestria omitido");
                            omitido = true;

                        }

                        //documentos extranjeros
                        if (extranjero == false && i == 15 || extranjero == false && i == 16) {
                            console.log("Documento extranjero omitido");
                            omitido = true;

                        }

                        //si no es caso especial
                        if (!omitido) {
                            console.log("Documento faltante: " + i);
                            todos = false;
                        }
                    }
                    //                            console.log(i + ": " + v);
                }

                //toma de decision
                if (masUno && todos) {
                    cambiarEstado(1);
                    console.log("Todos los documentos subidos");
                } else if (masUno && !todos) {
                    cambiarEstado(2);
                    console.log("Aun faltan documentos por subir");
                }
            } else location.href = "../login.php";
        }
    });
}

/*****************************************************
CAMBIA EL ESTADO DEL APARTADO DEPENDIENDO DE LOS
DOCUMENTOS FALTANTES Y LAS VALIDACIONES ESPECIALES
DEL PROGRAMA AL QUE SE ASPIRA
******************************************************/
function cambiarEstado(estado) {
    console.log("cambiando estado");
    var url = "./php/documentos.php?funcion=cambiarEstado&estado=" + estado; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            //si la sesion no se ha iniciado
            if (data != "salir") {
                if (estado == 1 && data == "Hecho") {

                    swal("Completado", "Se han guardado todos sus documentos correctamente.", "success", {
                            buttons: {
                                cancel: "Permanecer aqui",
                                catch: {
                                    text: "Ir al Inicio",
                                    value: "catch",
                                },
                                //                                        defeat: true,
                            },
                        })
                        .then((value) => {
                            switch (value) {

                                case "defeat":
                                    break;

                                case "catch":
                                    location.href = "index.html#about";
                                    break;

                                default:
                                    swal("Recuerde que puede sobrescribir un documento adjuntándolo nuevamente.");
                            }
                        });
                }
                if (estado == 2 && data == "Hecho") {
                    console.log("Aun faltan archivos por adjuntar siga así");
                    //                            alert("Aun faltan archivos por adjuntar siga asi");
                }
            } else location.href = "../login.php";
        }
    });
}

//        $("#btnEnviarPago").click(function () {
//            var $validator = $("#formularioBanco").validate();
//
//            var $valid = $("#formularioBanco").valid();
//
//            if ($valid) {
//                registrarPago();
//            } else {
//                $validator.focusInvalid();
//            }
//        });

/*****************************************************
REGISTRA LA INFORMACION ADICIONAL DE PAGO
******************************************************/
function registrarPago() {
    var url = "./php/documentos.php?funcion=registrarPago"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: $("#formularioBanco").serialize(), // Adjuntar los campos del formulario enviado.
        success: function (data) {
            //si la sesion no se ha iniciado
            if (data != "salir") {
                if (data == "Hecho") {} else {
                    swal("Lo sentimos", "No se guardaron sus datos por favor recargue la pagina e inténtelo de nuevo", "error", {
                        button: "Aceptar",
                    })
                }
            } else location.href = "../login.php";
        }
    });
}


/*****************************************************
ESTABLECE QUE DOCUMENTOS SE DEBEN ADJUNTAR DEPENDIENDO
DEL PROGRAMA AL QUE SE ASPIRA
******************************************************/
function consultarPrograma() {
    var url = "./php/documentos.php?funcion=consultarPrograma"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            if (data != "salir") {
                var datos = JSON.parse(data);

                $("#nombreAspirante").html(datos[0][0]);

                var programa = datos[0].nombre;
                nombre_programa = programa;
                trabaja=datos[0].trabaja;
                console.log("Trabaja: "+trabaja);
                //Si no es el doctorado
                if (programa != "Doctorado en Ciencias Biológicas") {
                    $("#campo_file_titulo_maestria").hide();
                    $("#campo_file_cedula_maestria").hide();
                    $("#campo_file_certificado_calificaciones_maestria").hide();
                    console.log("Se deshabilito titulo de maestria");
                    console.log("Se deshabilito cedula de maestria");
                    console.log("Se deshabilito certificado de maestria");
                }

                //Si no es MSPAS
                if (programa != "Maestría en Salud y Producción Animal Sustentable") {
                    $("#campo_file_carta_jefe_inmediato").hide();
                    console.log("Se deshabilito carta de jefe inmediato");
                }else if(trabaja=="No") {
                    $("#campo_file_carta_jefe_inmediato").hide();
                    console.log("Se deshabilito carta de jefe inmediato");
                }
                

                //Si no es MCB o DCB
                if (programa != "Doctorado en Ciencias Biológicas" && programa != "Maestría en Ciencias Biológicas") {
                    $("#campo_file_carta_aceptacion").hide();
                    $("#campo_file_resumen_anteproyecto").hide();
                    console.log("Se deshabilito la carta de aceptacion: " + programa);
                    console.log("Se deshabilito el resumen de anteproyecto: " + programa);
                }
                
                //Si es extranjero pero no es Doctorado
                if (datos[0].pais != "México" && programa != "Doctorado en Ciencias Biológicas") {
                    $("#campo_file_apostillado_certificado_maestria").hide();
                    console.log("Apostillado de Maestria oculto");
                }

                //Si no es extranjero
                if (datos[0].pais == "México") {
                    $("#col2").hide();
                    extranjero = false;
                    console.log("Datos adicionales ocultos");
                }
                //
                
                verificarArchivosFaltantes(); //Verificar si ya han sido subidos todos los documentos
                
            } else location.href = "../login.php";
        }
    });
}

/*****************************************************
Inicializar y configurar los tooltips
******************************************************/
$(function () {
  $('[data-toggle="tooltip"]').tooltip({
      delay: { "show": 300, "hide": 100 }
  })
});
