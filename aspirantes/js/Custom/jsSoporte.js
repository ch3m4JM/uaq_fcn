//Inicio de la pagina
$(document).ready(function () {
    validarUsuario();
});

//Variables globales
var email;
var Iam;

//Se valida la sesión y el tipo de usuario
function validarUsuario(){
    var url = "./php/soporte.php?funcion=validarUsuario"; // El script a dónde se realizarÃ¡ la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            if(data!="salir"){
                console.log("Iam: "+data);
                var datos = JSON.parse(data);
                var tipo=datos[0];
                Iam=tipo;
                email=datos[1];
                consultarPreguntas();
            }else{
                console.log("Salir");
                location.href="login.php";
            } 
        }
    });
}

//Al abrir el modal de una nueva pregunta
$("#modalTema").on("shown.bs.modal", function () {
    $("#inputPregunta").focus();
});

/*****************************************************
Registra una nueva pregunta
******************************************************/
$("#btnEnviarPregunta").click(function () {
    //console.log("Pais: " + pais);
    $('#modalTema').modal('hide');
    showDialog();
 
    var url = "./php/soporte.php?funcion=registrarPregunta&e="+email; // El script a dónde se realizarÃ¡ la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: $("#formularioPregunta").serialize(),
        success: function (data) {
            console.log(data);
            if (data == "Hecho") {
                $("#inputPregunta").empty();
                //document.getElementById("inputPregunta").reset();
                console.log(data);
                swal("Listo.", "Gracias por su colaboración, su duda sera respondida a la brevedad.", "success", {
                    button: "Aceptar"
                });
            }
            //location.href="#newCuestion";
            consultarPreguntas();
        }
    });
});

/*****************************************************
Muestra todas las preguntas
******************************************************/
function consultarPreguntas() {
    showDialog();
    //consultar preguntas
    var url = "./php/soporte.php?funcion=consultarPreguntas"; // El script a dónde se realizarÃ¡ la petición.
    $.ajax({
        type: "POST",
        url: url,
        //        data: $("#formularioPregunta").serialize(),
        success: function (data) {
                        console.log(data);
            if (data != "") {
                var preguntas = JSON.parse(data);

                //Limpiar campo de preguntas OPCIONAL REVIZAR ARRIBA
                $("#bodyPage").html("");

                console.log("Se limpio el campo de preguntas");

                //por cada pregunta
                $.each(preguntas, function (index) {
                    //Obtener datos de las preguntas
                    var usuario = preguntas[index].fk_usuario_correo;
                    var pregunta = preguntas[index].pregunta;
                    var fecha = preguntas[index].fecha;
                    var pkPregunta = preguntas[index].pk_pregunta;
                    var tipoUsuario = preguntas[index].tipo;
                    if (tipoUsuario != "aspirante") tipoUsuario= "Con privilegios"
                    else tipoUsuario="Aspirante";
                    //                console.log(usuario);
                    console.log("if: "+Iam);
                    if(Iam!="aspirante"){
                        if (pkPregunta != undefined) {
                            $("#bodyPage").append("<div class='container d-flex justify-content-center card-teme' id='pr_"+pkPregunta+"'><div class='col-md-7'><div class='card'><div class='card-header'><div class='row'><p><span class='fa-stack fa-sm text-info' id='iconoCuestionario'> <i class='fa fa-circle fa-stack-2x'></i><i class='fa fa-user-o fa-stack-1x fa-inverse'></i></span><span class='name-header'>" + usuario + "</span ></p><p><small class='text-muted text-fecha'>("+tipoUsuario+') ' + fecha + "</small></div></p><p class='text-center'><span class='cuestion-header'>" + pregunta + "</span ></p></div><div class='card-bloc' id='respuestas_" + pkPregunta + "'><small id='help_" + pkPregunta + "' class='text-muted text-fecha'>" + "Aún nadie a respondido este tema..." + "</small></div><div class='card-footer'><form id='form_answer_" + pkPregunta + "'><textarea class='form-control' placeholder='Responder...' required id='inputAnswer" + pkPregunta + "' name='inputAnswer'></textarea><div class='d-flex justify-content-end btn-responder' > <button class='btn btn-outline-primary btn-sm btn-respuesta' onClick='answer(" + pkPregunta + ")' value=" + pkPregunta + " id='btn_" + pkPregunta + "'><i class = 'fa fa-edit'> </i> Responder</button></form></div></div></div></div></div>");

                            //                    console.log("agregado");

                            consultarRespuestas(pkPregunta);
                        }
                    }else{
                        if (pkPregunta != undefined && tipoUsuario=="Aspirante") {
                            $("#bodyPage").append("<div class='container d-flex justify-content-center card-teme' id='pr_"+pkPregunta+"'><div class='col-md-7'><div class='card'><div class='card-header'><div class='row'><p><span class='fa-stack fa-sm text-info' id='iconoCuestionario'> <i class='fa fa-circle fa-stack-2x'></i><i class='fa fa-user-o fa-stack-1x fa-inverse'></i></span><span class='name-header'>" + usuario + "</span ></p><p><small class='text-muted text-fecha'>" + fecha + "</small></div></p><p class='text-center'><span class='cuestion-header'>" + pregunta + "</span ></p></div><div class='card-bloc' id='respuestas_" + pkPregunta + "'><small id='help_" + pkPregunta + "' class='text-muted text-fecha'>" + "Aún nadie a respondido este tema..." + "</small></div><div class='card-footer'><form id='form_answer_" + pkPregunta + "'><textarea class='form-control' placeholder='Responder...' required id='inputAnswer" + pkPregunta + "' name='inputAnswer'></textarea><div class='d-flex justify-content-end btn-responder' > <button class='btn btn-outline-primary btn-sm btn-respuesta' onClick='answer(" + pkPregunta + ")' value=" + pkPregunta + " id='btn_" + pkPregunta + "'><i class = 'fa fa-edit'> </i> Responder</button></form></div></div></div></div></div>");

                            //                    console.log("agregado");

                            consultarRespuestas(pkPregunta);
                        }
                    }
                });
                
                focusCuestion();
            }
                hideDialog();
            }
        
    });
}

/*****************************************************
Crea una respuesta a la publicación correspondiente
******************************************************/
function answer(fk_pregunta) {
    $validator = $("#form_answer_" + fk_pregunta).validate();

    var $valid = $("#form_answer_" + fk_pregunta).valid();

    if ($valid) {
        showDialog();
        document.getElementById("btn_" + fk_pregunta).setAttribute("disabled", "disabled");
        //document.getElementById("btn_" + fk_pregunta).setAttribute("disabled");

        //////////////Select Pais
        var url = "./php/soporte.php?funcion=registrarRespuesta&fkT=" + fk_pregunta+"&e="+email; // El script a dónde se realizarÃ¡ la petición.

        $.ajax({
            type: "POST",
            url: url,
            data: $("#form_answer_" + fk_pregunta).serialize(),
            success: function (data) {
                //console.log(data);
                if (data == "Hecho") {
                    //limpiar formulario de respuesta
                    $el = $("#form_answer_" + fk_pregunta).children("#inputAnswer" + fk_pregunta);
                    document.getElementById("form_answer_" + fk_pregunta).reset();

                    //Habiltar boton
                    var btn = document.getElementById("btn_" + fk_pregunta).getAttribute("disabled");

                    document.getElementById("btn_" + fk_pregunta).removeAttribute("disabled", "false");
                    swal("Listo.", "Gracias por su colaboración", "success", {
                        button: "Aceptar"
                    });
                }
                consultarRespuestas(fk_pregunta);
            }
        });
    } else {
        $validator.focusInvalid();
    };
};

/*****************************************************
Consultar las respuestas de cada Pregunta
******************************************************/
function consultarRespuestas(pkPregunta) {
    //consultar preguntas
    var url = "./php/soporte.php?funcion=consultarRespuestas&fkP=" + pkPregunta; // El script a dónde se realizarÃ¡ la petición.
    $.ajax({
        type: "POST",
        url: url,
        //        data: $("#formularioPregunta").serialize(),
        success: function (data) {
            //            console.log(data);
            if (data != null && data != "") {
                //                console.log(data);

                var respuestas = JSON.parse(data);
                $("#respuestas_" + pkPregunta).html("");
                //por cada pregunta
                $.each(respuestas, function (index) {
                    if (respuestas[index].fk_pregunta != undefined) {
                        //Obtener datos de las preguntas
                        var usuario = respuestas[index].fk_usuario_correo;
                        var respuesta = respuestas[index].respuesta;
                        var fecha = respuestas[index].fecha;
                        var fkPregunta = respuestas[index].fk_pregunta;
                        
                        if(usuario=="Desarrollador"){
                            var respuesta_formato = "<hr width='300px'><p class='text-info'><span class='fa-stack fa-sm text-info' id='iconoCuestionario'> <i class = 'fa fa-circle fa-stack-2x' ></i> <i class = 'fa fa-user-o fa-stack-1x fa-inverse' > </i> </span><span class='name-header'><strong>" + usuario + ": </strong> </span> <span class='name-header'>" + respuesta + "</span ></p>";
                        }else {
                            var respuesta_formato = "<hr width='300px'><p><span class='fa-stack fa-sm text-success' id='iconoCuestionario'> <i class = 'fa fa-circle fa-stack-2x' ></i> <i class = 'fa fa-user-o fa-stack-1x fa-inverse' > </i> </span><span class='name-header'><strong>" + usuario + ": </strong> </span> <span class='name-header'>" + respuesta + "</span ></p>";
                        }

                        $("#respuestas_" + pkPregunta).append(respuesta_formato);
                        $("#help_" + pkPregunta).hide();
                    }
                });
            }
            hideDialog();
        }
    });
}

/*****************************************************
Enfoca la pregunta cuando se accede desde el correo 
electrónico
******************************************************/
function focusCuestion() {
    var url = "./php/support-validate.php?f=focus"; // El script a dónde se realizarÃ¡ la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (numeroPregunta) {
            console.log(numeroPregunta);
            if (numeroPregunta != "") {
                location.href="#pr_"+numeroPregunta;
                $("#inputAnswer"+numeroPregunta).focus();
            }
        }
    });
}

/*****************************************************
MUESTRA EL GIF PROCESANDO
******************************************************/
function showDialog() {
    $("#bodyProcesando").addClass("procesando");
    $("#bodyPage").addClass("hide");
}

/*****************************************************
OCULTA EL GIF PROCESANDO
******************************************************/
function hideDialog() {
    $("#bodyProcesando").removeClass("procesando");
    $("#bodyPage").removeClass("hide");
}
